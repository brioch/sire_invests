import os
import numpy as np
import pandas as pd
from multiprocessing import shared_memory
import tables
import blosc
import pickle
import matplotlib.pyplot as plt


def _fill_realisation_response_fromnp(shr_name, arshape, reals, oeidx, oecols,
                                      hdfname,  fmt, fulld):
    """
    Poss low memory option by using array lodged in shared mem.
    However, seems slower than df version
    :param shr_name: name of shared memory entry
    :param arshape: shape of response ensemble stored on share
    :param reals: realisations to process
    :param oeidx: index of response ensemble (kijt+)
    :param oecols: columns of response ensemble (spike ijr)
    :param hdfname: Path base name for saving realisation based response
                        ensemble
    :param fmt: format for appending realisation number to saved response
                 ensemble file
    :param fulld: multiprocessing manger dict for storing a passing back progress
    :return:
    """
    print(f"Prep processor for reals {reals}")
    ij = oecols.droplevel('real').unique()
    ijdf = ij.to_frame()
    oecolsdf = oecols.to_frame()
    print("Accessing oe from memory buffer")
    existing_shm = shared_memory.SharedMemory(name=shr_name)
    oe = np.ndarray(arshape, dtype=np.float32, buffer=existing_shm.buf)
    print("oe retrieved from memory buffer")
    for r in reals:  # Loop over reals
        ijdf_temp = ijdf.copy()
        ijdf_temp.insert(2, 'real', r)
        rij = pd.MultiIndex.from_frame(ijdf_temp)
        fulldi = {}
        if fulld is None:
            print(f"Setting up response matrix container for realisation {r}")
            fa = pd.DataFrame(index=oeidx, columns=rij)
            # ids for realisation in obs ensemble
            sel = oecolsdf.real == r  # where entries for realisation does exist
            roecols = oecolsdf[sel].index  # ijr index value for realisations
            oeloc = np.concatenate(np.argwhere(
                sel.values))  # location of realisation entries in numpy array
            print(f"Initial fill of container for realisation {r}")
            # assign entries in oe for realisation to the full matrix for this real
            # this is MUCH quicker than fa.loc[:, roecols] = oe[:, oeloc]
            fa.update(
                pd.DataFrame(oe[:, oeloc], index=fa.index, columns=roecols))
        for i, j in ij:
            # may offer speed up (don't need to update where we have en response)
            if (i, j, r) not in oecols:
                # define search box - within this box look for a response result
                # adjust if i on edge -- but what if not true grid?
                # i might not be on edge but may not be a load cell...
                # so...
                # location of currrent spike cell in oe, for all reals
                # selection of all reals for current spike
                sel = (oecolsdf.i == i) & (oecolsdf.j == j)
                # numpy location for all reals for current spike
                sploc = np.concatenate(np.argwhere(sel.values))
                # mean for realisation where we do have the spike response
                # for the current spike cell
                ijmeanresp = oe[:, sploc].mean(axis=1)
                sw = 3 // 2  # TODO numerator need to be discovered -- or could just be 1, might be robust, just req more adjust in loop
                # loop adjusting search width to find nearby spike cell
                # to use as reference for realisation
                while True:
                    # define search range
                    iis = range(i - sw, i + sw + 1)
                    jjs = range(j - sw, j + sw + 1)
                    # will this match -- dealing with pandas erroring if keys missing
                    if not any([(ii, jj, r) in oecols for ii in iis for jj in
                                jjs]):
                        # need to expand search
                        # -- will re-enter loop with new search width
                        print(
                            f"real:{r} for spike:{(i, j)} "
                            f"not found in search box rows:{iis}, "
                            f"cols:{jjs}... expanding search")
                        sw += 1  # expand search
                    else:
                        # get pd selection for representative spike responses
                        sel = (oecolsdf.i.isin(iis) &
                               oecolsdf.j.isin(jjs) &
                               (oecolsdf.real == r))
                        swapids = oecolsdf[sel].index
                        # location index in numpy array
                        swaploc = np.concatenate(np.argwhere(sel.values))
                        print(
                            f"filling (i, j, r):{(i, j, r)} "
                            f"with mean in search box rows:{iis}, "
                            f"cols:{jjs}")
                        # get a representivitve response value for realisation
                        #  by using adjacent spike cells
                        si = swapids.get_level_values(
                            'i')  # spike row value to swap in
                        sj = swapids.get_level_values(
                            'j')  # spike column values to swap in
                        # all realisations associated with the representative data
                        sel = (oecolsdf.i.isin(si) &
                               oecolsdf.j.isin(sj))
                        # allrealids = oecolsdf[sel].index
                        allrealloc = np.concatenate(np.argwhere(sel.values))
                        # For each ij in swaps there should be the same number of reals
                        # check this
                        assert len(oecolsdf[sel].index.droplevel(
                            'real').unique()) * len(
                            oecolsdf[sel].index.get_level_values(
                                'real').unique()) == len(oecolsdf[sel]), (
                            "dimensions of number of *adjacent* cells and "
                            "number of representative realisations not equal")
                        # This means that the mean across all ijr can be used. (oe[:, allrealloc].mean(axis=1))
                        # Scale representative response by multiplying by
                        # (mean response for current spike cell)/(mean response for representaive cells)
                        # swap@r * (sp@all/swap@all)
                        # DIVIDE mean for realisations where we do have the spike reponse for the current spike cell
                        # BY     mean spike response for *adjacent* spike cells
                        scale = (ijmeanresp / oe[:, allrealloc].mean(axis=1))
                        # calc mean spike response for *adjacent* cells for
                        # current (missing) realisation
                        repres = oe[:, swaploc].mean(axis=1) * scale
                        # but need to deal with cases where *adjacent* doesn't
                        # ever cause a response
                        # USE mean for realisation where we do have the spike
                        # response for the current spike cell
                        repres[np.isnan(repres)] = ijmeanresp[np.isnan(repres)]
                        if fulld is None:
                            fulldi[(i, j, r)] = repres  # only dict fill for this real
                        else:
                            fulld[(i, j, r)] = repres  # rapid dictionary fill
                        break
        if fulld is None:
            print(f"Building infill response df for real {r}...")
            udf = pd.DataFrame.from_dict(fulldi).set_index(oeidx)
            udf.columns = udf.columns.rename(['i', 'j', 'real'])
            print(f"Filling full response df for real {r}...")
            fa.update(udf, overwrite=False)
            fa = fa.astype('float32').sort_index(axis=1, level=['i', 'j'])
            # ^ ensureing column order is preserved and data stored as float32 ^
            # some mem cleanup
            udf = None
            fulldi = None
            if hdfname:  # safer to write separate files when multiprocessing?
                # hdfnamer = hdfname.replace('.hdf', f"{r:{fmt}}.hdf")
                # print(f"Writing response matrix hdf for real {r}: {hdfname}")
                # fa.astype('float32').to_hdf(hdfnamer, 'df', mode='w',
                #                             complib='blosc:lz4hc', complevel=9)
                # OR as simple arrays to hdf -- this might allow numpy load response calcs
                hdfnamer = hdfname.replace('.hdf', f"_{r:{fmt}}.hdf")
                with tables.open_file(
                        hdfnamer, mode="w",
                        filters=tables.Filters(complevel=9,
                                               complib='blosc:lz4hc')
                ) as st:
                    st.create_carray('/', f'r_{r}', obj=fa.values)
            fa = None
    existing_shm.close()


def _fill_realisation_response_fromdf(reals, oe, meanoe, fname, fmt, fulld):
    """
    Infilling response for set of realisation. Memory intensive.
    Saving on memory by only passing slice of response matrix ensemble
    (for req reals). To do this all need the mean of the response ensemble
    across ij (meanoe) -- this is used for scaling the infill.
    :param reals: realisations to process
    :param oe: Response ensemble DataFrame -- pre sliced for required reals.
    :param meanoe: Mean of response for each ij (across all simulated
                    realisations)
    :param hdfname: Path base name for saving realisation based response
                        ensemble
    :param fmt: format for appending realisation number to saved response
                 ensemble file
    Writes response matrix for each realisation to hdf file.
    """
    print(f"Prep processor for reals {reals}")
    ij = meanoe.columns  # mean already has realisation level dropped.
    ijdf = ij.to_frame()  # used for querying later
    sumfa = pd.DataFrame(index=oe.index, columns=ij, data=0)
    hdfname = None
    blcname = None
    if fname is not None:
        if 'hdf' in fname:
            hdfname = fname
            blcname = None
        elif 'blc' in fname:
            hdfname = None
            blcname = fname
    for r in reals:  # Loop over reals
        # Building dataframe container for this realisation
        ijdf_temp = ijdf.copy()  # Adding level so safer to copy first
        ijdf_temp.insert(2, 'real', r)
        rij = pd.MultiIndex.from_frame(ijdf_temp)
        fulldi = {}  # init dict for storing infil responses
        if fulld is None:
            fa = pd.DataFrame(index=oe.index, columns=rij)
            # Adding responses that are present for realisation
            fa.update(oe.loc[:, (slice(None), slice(None), r)])
        for i, j in ij:
            # may offer speed up (don't need to update where we have en response)
            if (i, j, r) not in oe.columns:
                # define search box - within this box look for a response result
                # adjust if i on left hand edge -- but what if not true grid?
                # i might not be in left hand edge but may not be a load cell...
                # so...
                # mean for realisation where we do have the spike response
                # for the current spike cell
                ijmeanresp = meanoe.loc[:, (i, j)]
                sw = 3 // 2  # TODO numerator need to be discovered -- or could just be 1, might be robust, just req more adjust in loop
                # loop adjusting search width to find nearby spike cell
                # to use as reference for realisation
                while True:
                    # define search range
                    iis = range(i - sw, i + sw + 1)
                    jjs = range(j - sw, j + sw + 1)
                    # will this match -- dealing with pandas erroring if keys missing
                    if not any(
                            [(ii, jj, r) in oe.columns for ii in iis for jj in
                             jjs]):
                        # need to expand search
                        # -- will re-enter loop with new search width
                        print(
                            f"real:{r} for spike:{(i, j)} "
                            f"not found in search box rows:{iis}, "
                            f"cols:{jjs}... expanding search")
                        sw += 1  # expand search
                    else:
                        # ID columns that will be used to infill
                        swapcols = oe.loc[:, (iis, jjs, r)].columns
                        print(
                            f"filling (i, j, r):{(i, j, r)} "
                            f"with mean in search box rows:{iis}, "
                            f"cols:{jjs}")
                        # get a representivitve response value for realisation
                        #  by using adjacent spike cells cells
                        # spike row value to swap in
                        si = swapcols.get_level_values('i')
                        # spike column values to swap in
                        sj = swapcols.get_level_values('j')
                        # scale representative response by multiplying by
                        # (mean response for current spike cell)/(mean response for representaive cells)
                        # swap@r * (sp@all/swap@all)
                        # DIVIDE mean for realisations where we do have the spike response for the current spike cell
                        # BY     mean spike response for *adjacent* spike cells
                        scale = meanoe.loc[:, (si, sj)].divide(
                            ijmeanresp, axis=0)**-1  # keep swap cell separate for now
                        # calc mean spike response for *adjacent* cells for
                        # current (missing) realisation.
                        # Also dealing with cases where *adjacent* doesn't
                        # ever cause a response
                        # USE mean for realisation where we do have the spike
                        # response for the current spike cell
                        repres = oe.loc[:, (si, sj, r)].mul(
                            scale).replace(
                            [np.inf, -np.inf], np.nan).mean(axis=1).fillna(ijmeanresp)
                        # We may have issues mapping responses for spiked cells
                        #   themselves.
                        # Mapped cells might have too big a response,
                        #   host cells may have too small a response.
                        # Get swap cell responses for real, relative to mean
                        #   k zero because spike only in top layer
                        bsscaler = (oe.loc[(0, si, sj), (si, sj, r)].droplevel(
                            'real', axis=1)/meanoe.loc[(0, si, sj), (si, sj)])
                        # speed mask,
                        #   so we can just get cell response for spike cell
                        mask = bsscaler.index.droplevel(
                            [l for l in bsscaler.index.names
                             if l not in {'i', 'j'}]
                        ).values[:, None] != bsscaler.columns.values[None, :]
                        # apply mask and mean (will reduce df)
                        bsscaler = bsscaler.mask(mask).mean(axis=1).dropna()
                        # apply scaling to mean response for current cell
                        boostmat = (ijmeanresp.loc[(0, i, j)] *
                                    bsscaler.groupby('t').mean())
                        # apply scaling to mean response to swap cells
                        supmat = ijmeanresp.loc[(0, si, sj)].mul(
                            bsscaler).dropna()
                        # infill response for spike cell and real
                        repres.loc[(0, i, j)] = boostmat.values
                        repres.loc[supmat.index] = supmat
                        if fulld is None:
                            fulldi[(i, j, r)] = repres
                        else:
                            fulld[(i, j, r)] = repres  # rapid dictionary fill
                        break
        if fulld is None:
            print(f"Building infill response df for real {r}...")
            udf = pd.DataFrame.from_dict(fulldi)
            udf.columns = udf.columns.rename(['i', 'j', 'real'])
            print(f"Filling full response df for real {r}...")
            fa.update(udf, overwrite=False)
            fa = fa.astype('float32').sort_index(axis=1, level=['i', 'j'])
            # ^ ensureing column order is preserved and data stored as float32 ^
            # some mem cleanup
            udf = None
            fulldi = None
            sumfa = sumfa + fa.droplevel('real', axis=1)
            if hdfname:  # safer to write separate files when multiprocessing?
                # hdfnamer = hdfname.replace('.hdf', f"{r:{fmt}}.hdf")
                # print(f"Writing response matrix hdf for real {r}: {hdfname}")
                # fa.astype('float32').to_hdf(hdfnamer, 'df', mode='w',
                #                             complib='blosc:lz4hc', complevel=9)
                # OR as simple arrays to hdf -- this might allow numpy load response calcs
                print(f"Writing response matrix for real {r}")
                hdfnamer = hdfname.replace('.hdf', f"_{r:{fmt}}.hdf")
                with tables.open_file(
                        hdfnamer, mode="w",
                        filters=tables.Filters(complevel=9,
                                               complib='blosc:lz4hc')
                ) as st:
                    st.create_carray('/', f'r_{r}', obj=fa.values)
            if blcname:  # quick loading of blosc compressed and dumped
                print(f"Writing response matrix to blc for real {r}")
                fa = np.ascontiguousarray(fa.values, np.float32)
                blcnamer = blcname.replace('.blc', f"_{r:{fmt}}.blc")
                c = blosc.compress_ptr(fa.__array_interface__['data'][0],
                                       fa.size,
                                       fa.dtype.itemsize,
                                       clevel=9, cname='zstd',
                                       shuffle=blosc.SHUFFLE)
                with open(blcnamer, 'wb') as fp:
                    pickle.dump((fa.shape, fa.dtype), fp)
                    fp.write(c)
            fa = None
    return sumfa


def _read_and_apply_sire(c, stop, deltal, respdict):
    for f in c:
        bn = os.path.basename(f)
        print(f"Applying load to {bn}")
        r = int(bn.strip('.hdf').split('_')[-1])
        with tables.open_file(f, 'r') as h5_file:
            # respdict.append(pd.Series(h5_file.root[f'r_{r}'].read(stop=stop).dot(
            #     deltal)))
            respdict.append(pd.Series(h5_file.root[f'r_{r}'].read(stop=stop).dot(
                deltal), name=r))
            # respdict.update(
            #     {r: h5_file.root[f'r_{r}'].read(stop=stop).dot(
            #         deltal)})
