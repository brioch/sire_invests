"""

"""
import pyemu
import flopy
import os
import numpy as np
import random
import shutil
import pandas as pd
import matplotlib.pyplot as plt
import sys
sys.path.append('..')
# from transient_sire import sire
from transient_sire import sire
import string
import subprocess
import tables
import multiprocessing as mp
from multiprocessing import shared_memory, Lock
import time


def load2crch(d='.'):
    load_arr = np.loadtxt(os.path.join(d, 'load.dat'))
    kpers = [f.strip('.ref').strip('rech_') for f in os.listdir(d)
             if f.startswith('rech_')]
    n_crch = {}
    for kper in kpers:
        rech_arr = np.loadtxt(os.path.join(d, f"rech_{kper}.ref")) * 250 * 250
        concen_arr = load_arr / rech_arr
        concen_arr[np.isnan(concen_arr)] = 0.0
        concen_arr[np.isinf(concen_arr)] = 0.0
        concen_arr[concen_arr < 0.0] = 0.0
        concen_arr[concen_arr > 10.0] = 10.0
        np.savetxt(os.path.join(d, f'crch1{kper}.ref'), concen_arr,
                   fmt="%15.6E", delimiter="")
        n_crch[kper] = concen_arr
    return n_crch


def setup_freyberg_models():
    """
    quick mf and mt model set up
    :return:
    """
    m = flopy.modflow.Modflow.load(
        'freyberg.nam',
        model_ws=os.path.join(
            pyemu.__path__[0], '..', 'examples', 'Freyberg_transient'),
        version='mfnwt')
    lmt = flopy.modflow.ModflowLmt(m, package_flows=['SFR'])
    sfr = flopy.modflow.ModflowSfr2.load(
        os.path.join(m.model_ws, 'freyberg.sfr'), m)
    m.start_datetime = '2015-01-01'
    m.external_path = '.'
    m.change_model_ws('freyberg')
    # write m
    m.write_input()

    # transport model build
    mt = flopy.mt3d.Mt3dms('freyberg_mt', modflowmodel=m, version='mt3d-usgs',
                           model_ws='freyberg')
    nper = m.nper
    nrow = m.nrow
    ncol = m.ncol
    perlen = m.dis.perlen.array.copy()
    nstp = np.zeros((nper)) + 1
    tsmult = np.zeros((nper)) + 1
    flopy.mt3d.Mt3dBtn(mt, MFStyleArr='True', DRYCell='True',
                       icbund=m.bas6.ibound.array, prsity=0.1, sconc=0,
                       perlen=perlen, nper=nper, ncomp=1, mcomp=1,
                       nstp=nstp, tsmult=tsmult)
    flopy.mt3d.Mt3dGcg(mt, mxiter=50, iter1=50, )
    flopy.mt3d.Mt3dAdv(mt, mixelm=0, percel=0.75)
    flopy.mt3d.Mt3dDsp(mt, al=0.01, trpt=0.1)
    # random reaction distro
    rc1 = np.array([[random.choice((0.007, 0.002, 0.0)) for _ in range(m.ncol)]
                    for _ in range(m.nrow)])
    flopy.mt3d.Mt3dRct(mt, isothm=0, ireact=1, igetsc=0, rc1=rc1)
    flopy.mt3d.Mt3dSft(mt, nsfinit=abs(m.sfr.nstrm), mxsfbc=0, ietsfr=0, wimp=1,
                       ioutobs=1001, nobssf=abs(m.sfr.nstrm),
                       obs_sf=np.arange(abs(m.sfr.nstrm)) + 1, iprtxmd=1)
    # dirty quick random load
    np.random.seed(20)
    base_load_arr = 1 + (50 * np.random.random([nrow, ncol]))
    # base_load_arr = np.ones([nrow, ncol]) * 10
    np.savetxt(os.path.join(mt.model_ws, 'load.dat'), base_load_arr)
    n_crch = load2crch(mt.model_ws)
    ssm = flopy.mt3d.Mt3dSsm(mt, crch=n_crch)
    # write mt
    mt.external_path = '.'
    mt.write_input()

    # run both
    shutil.copy(os.path.join('..', 'exe', 'mfnwt.exe'), 'freyberg')
    shutil.copy(os.path.join('..', 'exe', 'mt3dusgs.exe'), 'freyberg')
    pyemu.helpers.run(
        "{0} {1}".format("mfnwt", "freyberg.nam"),
        cwd=m.model_ws)
    pyemu.helpers.run(
        "{0} {1}".format("mt3dusgs", "freyberg_mt.nam"),
        cwd=mt.model_ws)


def difference_mtsim_outputs():
    """
    generate difference obs from processed (pyemu) model outputs
    mt3d001.ucn.dat - mt3d001.oucn.dat
    MT3D001.UCN_timeseries.processed - MT3D001.oUCN_timeseries.processed
    hauraki_transient.mt3d.processed - hauraki_transient.omt3d.processed
    :return:
    """
    for nfnme, ofnme, dfnme, idx_cols in [
        ["mt3d001.ucn.dat", "mt3d001.oucn.dat", "mt3d001.ducn.dat", [0]]]:
        ndf = pd.read_csv(nfnme, sep=' ', index_col=idx_cols)
        odf = pd.read_csv(ofnme, sep=' ', index_col=idx_cols)
        ddf = ndf - odf
        ddf.loc[:, ndf.columns].to_csv(dfnme, index=True, header=True, sep=' ')


def freyberg_pst(nreals=30):
    org_model_ws = os.path.join("freyberg")
    nam_file = "freyberg.nam"
    m = flopy.modflow.Modflow.load(nam_file, model_ws=org_model_ws,
                                   check=False, forgive=False,
                                   exe_name='mfnwt.exe')
    # flopy.modflow.ModflowRiv(m, stress_period_data={
    #     0: [[0, 0, 0, m.dis.top.array[0, 0], 1.0, m.dis.botm.array[0, 0, 0]],
    #         [0, 0, 1, m.dis.top.array[0, 1], 1.0, m.dis.botm.array[0, 0, 1]],
    #         [0, 0, 1, m.dis.top.array[0, 1], 1.0, m.dis.botm.array[0, 0, 1]]]})
    #
    # welsp = m.wel.stress_period_data.data.copy()
    # addwell = welsp[0].copy()
    # addwell['k'] = 1
    # welsp[0] = np.rec.array(np.concatenate([welsp[0], addwell]))
    # samewell = welsp[1].copy()
    # samewell['flux'] *= 10
    # welsp[1] = np.rec.array(np.concatenate([welsp[1], samewell]))
    # m.wel.stress_period_data = welsp
    #
    # org_model_ws = "temp_pst_from"
    # if os.path.exists(org_model_ws):
    #     shutil.rmtree(org_model_ws)
    # m.external_path = "."
    # m.change_model_ws(org_model_ws)
    # m.write_input()

    # for exe in [mf_exe_path, mt_exe_path, ies_exe_path]:
    #     shutil.copy(os.path.relpath(exe, '..'), org_model_ws)

    # print("{0} {1}".format(mf_exe_path, m.name + ".nam"), org_model_ws)
    # os_utils.run("{0} {1}".format(mf_exe_path, m.name + ".nam"),
    #              cwd=org_model_ws)
    ucn_kperk = []
    for k in range(m.nlay):
        for kper in [0, m.nper-1]:#range(m.nper):
            ucn_kperk.append([kper, k])
    ucn_runline, df = pyemu.gw_utils.setup_hds_obs(
        os.path.join(m.model_ws, 'MT3D001.UCN'), kperk_pairs=ucn_kperk,
        skip=None, prefix="ucn", include_path=False)


    template_ws = "new_temp"
    # sr0 = m.sr
    sr = pyemu.helpers.SpatialReference.from_namfile(
        os.path.join(m.model_ws, m.namefile),
        delr=m.dis.delr, delc=m.dis.delc)
    # set up PstFrom object
    pf = pyemu.utils.PstFrom(original_d=org_model_ws, new_d=template_ws,
                             remove_existing=True,
                             longnames=True, spatial_reference=sr,
                             zero_based=False)
    with open(os.path.join(template_ws, 'runcount.dat'), 'w') as f:
        f.write('1 0')
    pf.add_parameters(filenames='runcount.dat', par_type='grid',
                      transform='none', par_name_base='runid',
                      par_style='direct',  index_cols=0, use_cols=1,
                      alt_inst_str='')
    # pf.extra_py_imports.append('flopy')
    # pf.mod_sys_cmds.append("which python")
    # obs
    #   using tabular style model output
    #   (generated by pyemu.gw_utils.setup_hds_obs())
    pf.add_observations_from_ins('MT3D001.UCN.dat.ins')
    pf.post_py_cmds.append(ucn_runline)
    pf.tmp_files.append(f"MT3D001.UCN")

    # pars
    v = pyemu.geostats.ExpVario(contribution=1.0, a=2500)
    geostruct = pyemu.geostats.GeoStruct(variograms=v, transform='log')
    # Pars for river list style model file, every entry in columns 3 and 4
    # specifying formatted model file and passing a geostruct  # TODO method for appending specific ult bounds
    # pf.add_parameters(filenames="RIV_0000.dat", par_type="grid",
    #                   index_cols=[0, 1, 2], use_cols=[3, 4],
    #                   par_name_base=["rivstage_grid", "rivcond_grid"],
    #                   mfile_fmt='%10d%10d%10d %15.8F %15.8F %15.8F',
    #                   geostruct=geostruct, lower_bound=[0.9, 0.01],
    #                   upper_bound=[1.1, 100.], ult_lbound=[0.3, None])
    # # 2 constant pars applied to columns 3 and 4
    # # this time specifying free formatted model file
    # pf.add_parameters(filenames="RIV_0000.dat", par_type="constant",
    #                   index_cols=[0, 1, 2], use_cols=[3, 4],
    #                   par_name_base=["rivstage", "rivcond"],
    #                   mfile_fmt='free', lower_bound=[0.9, 0.01],
    #                   upper_bound=[1.1, 100.], ult_lbound=[None, 0.01])
    # Pars for river list style model file, every entry in column 4
    # pf.add_parameters(filenames="RIV_0000.dat", par_type="grid",
    #                   index_cols=[0, 1, 2], use_cols=[4],
    #                   par_name_base=["rivcond_grid"],
    #                   mfile_fmt='%10d%10d%10d %15.8F %15.8F %15.8F',
    #                   geostruct=geostruct, lower_bound=[0.01],
    #                   upper_bound=[100.], ult_lbound=[None])
    # constant par applied to column 4
    # this time specifying free formatted model file
    # pf.add_parameters(filenames="RIV_0000.dat", par_type="constant",
    #                   index_cols=[0, 1, 2], use_cols=[4],
    #                   par_name_base=["rivcond"],
    #                   mfile_fmt='free', lower_bound=[0.01],
    #                   upper_bound=[100.], ult_lbound=[0.01])
    # pf.add_parameters(filenames="RIV_0000.dat", par_type="constant",
    #                   index_cols=[0, 1, 2], use_cols=5,
    #                   par_name_base="rivbot",
    #                   mfile_fmt='free', lower_bound=0.9,
    #                   upper_bound=1.1, ult_ubound=100.,
    #                   ult_lbound=0.001)
    # setting up temporal variogram for correlating temporal pars
    date = m.dis.start_datetime
    v = pyemu.geostats.ExpVario(contribution=1.0, a=180.0)  # 180 correlation length
    t_geostruct = pyemu.geostats.GeoStruct(variograms=v, transform='log')
    # looping over temporal list style input files
    # setting up constant parameters for col 3 for each temporal file
    # making sure all are set up with same pargp and geostruct (to ensure correlation)
    # Parameters for wel list style
    well_mfiles = [f for f in os.listdir(template_ws) if f.startswith('WEL_')]
    for t, well_file in enumerate(well_mfiles):
        # passing same temporal geostruct and pargp,
        # date is incremented and will be used for correlation with
        pf.add_parameters(filenames=well_file, par_type="constant",
                          index_cols=[0, 1, 2], use_cols=3,
                          par_name_base="flux", alt_inst_str='kper',
                          datetime=date, geostruct=t_geostruct,
                          pargp='wellflux_t', lower_bound=0.25,
                          upper_bound=1.75)
        date = (pd.to_datetime(date) +
                pd.DateOffset(m.dis.perlen.array[t], 'day'))
    # par for each well (same par through time)
    pf.add_parameters(filenames=well_mfiles,
                      par_type="grid", index_cols=[0, 1, 2], use_cols=3,
                      par_name_base="welflux_grid",
                      zone_array=m.bas6.ibound.array,
                      geostruct=geostruct, lower_bound=0.25, upper_bound=1.75)
    # global constant across all files
    pf.add_parameters(filenames=well_mfiles,
                      par_type="constant",
                      index_cols=[0, 1, 2], use_cols=3,
                      par_name_base=["flux_global"],
                      lower_bound=0.25, upper_bound=1.75)

    # Spatial array style pars - cell-by-cell
    hk_files = ["hk_Layer_{0:d}.ref".format(i) for i in range(1, 4)]
    for hk in hk_files:
        pf.add_parameters(filenames=hk, par_type="grid",
                          zone_array=m.bas6.ibound[0].array,
                          par_name_base="hk", alt_inst_str='lay',
                          geostruct=geostruct,
                          lower_bound=0.01, upper_bound=100.)
    # Spatial array style pars - cell-by-cell
    vk_files = ["vka{0:d}.ref".format(i) for i in range(1, 4)]
    for vk in vk_files:
        pf.add_parameters(filenames=vk, par_type="grid",
                          zone_array=m.bas6.ibound[0].array,
                          par_name_base="vk", alt_inst_str='lay',
                          geostruct=geostruct,
                          lower_bound=0.01, upper_bound=100.)
    # Spatial array style pars - cell-by-cell
    ss_files = ["ss_Layer_{0:d}.ref".format(i) for i in range(1, 4)]
    for ss in ss_files:
        pf.add_parameters(filenames=ss, par_type="grid",
                          zone_array=m.bas6.ibound[0].array,
                          par_name_base="ss", alt_inst_str='lay',
                          geostruct=geostruct,
                          lower_bound=0.01, upper_bound=100.)

    # Pars for temporal array style model files
    date = m.dis.start_datetime  # reset date
    rch_mfiles = [f for f in os.listdir(template_ws) if f.startswith('rech_')]
    for t, rch_file in enumerate(rch_mfiles):
        # constant par for each file but linked by geostruct and pargp
        pf.add_parameters(filenames=rch_file, par_type="constant",
                          zone_array=m.bas6.ibound[0].array,
                          par_name_base="rch", alt_inst_str='kper',
                          datetime=date, geostruct=t_geostruct,
                          pargp='rch_t', lower_bound=0.9, upper_bound=1.1)
        date = (pd.to_datetime(date) +
                pd.DateOffset(m.dis.perlen.array[t], 'day'))
    # spatially distributed array style pars - cell-by-cell
    pf.add_parameters(filenames=rch_mfiles, par_type="grid",
                      zone_array=m.bas6.ibound[0].array,
                      par_name_base="rch",
                      geostruct=geostruct, lower_bound=0.9, upper_bound=1.1)
    # pf.add_parameters(filenames=rch_mfiles, par_type="pilot_point",
    #                   zone_array=m.bas6.ibound[0].array,
    #                   par_name_base="rch", pp_space=1,
    #                   ult_ubound=None, ult_lbound=None,
    #                   geostruct=geostruct, lower_bound=0.9, upper_bound=1.1)
    # global constant recharge par
    pf.add_parameters(filenames=rch_mfiles, par_type="constant",
                      zone_array=m.bas6.ibound[0].array,
                      par_name_base="rch_global", lower_bound=0.9,
                      upper_bound=1.1)
    # zonal recharge pars
    # pf.add_parameters(filenames=rch_mfiles,
    #                   par_type="zone", par_name_base='rch_zone',
    #                   lower_bound=0.9, upper_bound=1.1, ult_lbound=1.e-6,
    #                   ult_ubound=100.)

    # Spatial array style pars - cell-by-cell
    poro_files = ["prsity_layer_{0:d}.ref".format(i) for i in range(1, 4)]
    for poro in poro_files:
        pf.add_parameters(filenames=poro, par_type="grid",
                          zone_array=m.bas6.ibound[0].array,
                          par_name_base="poro", alt_inst_str='lay',
                          geostruct=geostruct,
                          lower_bound=0.3, upper_bound=3)
    rc11_files = ["rc11_layer_{0:d}.ref".format(i) for i in range(1, 4)]
    for rc11 in rc11_files:
        pf.add_parameters(filenames=rc11, par_type="grid",
                          zone_array=m.bas6.ibound[0].array,
                          par_name_base="rc11", alt_inst_str='lay',
                          geostruct=geostruct,
                          lower_bound=0.1, upper_bound=10)
    pf.add_parameters('load.dat', par_type="grid",
                      zone_array=m.bas6.ibound[0].array,
                      par_name_base="load",
                      geostruct=None,
                      lower_bound=0.1, upper_bound=10
                      )
    # add model run command
    pf.mod_sys_cmds.append(f"mfnwt.exe {m.name}.nam")
    pf.mod_sys_cmds.append(f"mt3dusgs freyberg_mt.nam")
    print(pf.mult_files)
    print(pf.org_files)
    pf.add_py_function('freyberg_sire_invest.py', "load2crch(d='.')")

    # build pest
    pst = pf.build_pst('freyberg.pst')
    # cov = pf.build_prior(fmt="ascii")
    pe = pf.draw(nreals, use_specsim=True)
    # check mult files are in pst input files
    csv = os.path.join(template_ws, "mult2model_info.csv")
    df = pd.read_csv(csv, index_col=0)
    mults_not_linked_to_pst = ((set(df.mlt_file.unique()) -
                                set(pst.input_files)) -
                               set(df.mlt_file))
    assert len(mults_not_linked_to_pst) == 0, print(mults_not_linked_to_pst)
    #
    # pst.write_input_files(pst_path=pf.new_d)
    # # test par mults are working
    # b_d = os.getcwd()
    # os.chdir(pf.new_d)
    # try:
    #     pyemu.helpers.apply_list_and_array_pars(
    #         arr_par_file="mult2model_info.csv")
    # except Exception as e:
    #     os.chdir(b_d)
    #     raise Exception(str(e))
    # os.chdir(b_d)
    #
    # pst.control_data.noptmax = 0
    # pst.write(os.path.join(pf.new_d, "freyberg.pst"))
    # pyemu.os_utils.run("{0} freyberg.pst".format(ies_exe_path), cwd=pf.new_d)
    #
    # res_file = os.path.join(pf.new_d, "freyberg.base.rei")
    # assert os.path.exists(res_file), res_file
    # pst.set_res(res_file)
    # print(pst.phi)
    # assert pst.phi < 1.0e-5, pst.phi

    # set run id
    idcol = [c for c in pe.columns if c.startswith('runid')][0]
    pe.loc[:, idcol] = pe.index.values + 1
    pe.to_binary(os.path.join(pf.new_d, 'par.jcb'))
    #
    # quick sweep test?
    pst.pestpp_options["ies_par_en"] = 'par.jcb'
    pst.pestpp_options["ies_num_reals"] = nreals
    pst.control_data.noptmax = -1
    # par = pst.parameter_data
    # par.loc[:, 'parval1'] = pe.iloc[0].T
    pst.write(os.path.join(pf.new_d, "freyberg.pst"))
    # pyemu.os_utils.run("{0} freyberg.pst".format(ies_exe_path), cwd=pf.new_d)
    # pyemu.os_utils.start_workers(pf.new_d,
    #                              exe_rel_path="pestpp-ies",
    #                              pst_rel_path="freyberg.pst",
    #                              num_workers=20, master_dir="master",
    #                              cleanup=False, port=4005)
    #Quick test
    # os.chdir(pf.new_d)
    # pyemu.helpers.apply_list_and_array_pars()
    # os.chdir('..')


def extract_loadreal(nreal):
    realid = np.loadtxt('runcount.dat')[1]
    loadreal = np.floor_divide(int(realid), nreal)+1
    return int(loadreal)


def prep_for_response_ensemble_unc(r_d='.', t_d='template_respunc',
                                   spike_mag=10, nloadreals=10,
                                   seed_randreals=True, enstype='basic',
                                   uniformbase=True):
    """
    Build PEST control file for ensemble simulations to provide change in nitrate concentration uncertainty
    0. Start with transient transport pest interface (template_transient)
    1. Set flow simulation to scenario sim
    2. Setup 2 transport sims:
        2a. One with no loading
        2b. One with loading
    3. Setup difference obs
    :return:
    """
    import string
    it = 3
    # pstfile = 'hauraki.pst'
    it_d = os.path.join(r_d, 'new_temp')
    # t_d = 'template_respunc'
    # fsim_d = 'temp_future_transient'
    if os.path.exists(t_d):
        shutil.rmtree(t_d)
    print(f"Copying PEST template dir to {t_d}")
    shutil.copytree(it_d, t_d)
    print("Loading par ensembles")
    pstfile = 'freyberg.pst'
    pst = pyemu.Pst(os.path.join(t_d, pstfile))
    pe = pyemu.ParameterEnsemble.from_binary(pst, os.path.join(t_d, f'par.jcb'))
    # pst, pe = _prep_response_interface(it_d, t_d, en_d, it)
    mult = 100.
    if enstype == 'basic':
        nloadreals = 1
    elif enstype.startswith('spikerange'):
        mult = 0.
    base_arr = np.loadtxt(os.path.join(t_d, "load.dat"))
    np.savetxt(os.path.join(t_d, "load.dat.orig"), base_arr)
    if uniformbase:
        o_arr = np.ones(base_arr.shape) * mult
    else:
        o_arr = base_arr
    np.savetxt(os.path.join(t_d, "oload.dat"), o_arr)

    # fix load pars
    load_pars = [c for c in pe.columns if 'load' in c]
    pe.loc[:, load_pars] = 1.
    # Expand pe for internal load reals
    exp_pe = pd.DataFrame(np.tile(pe.values, (nloadreals, 1)),
                          columns=pe.columns)
    # Set real id
    idcol = [c for c in exp_pe.columns if c.startswith('runid')][0]
    exp_pe.loc[:, idcol] = exp_pe.index.values
    # Need to setup to run two mt sims.
    # one with without loading
    # one with universal unit load...
    # dummy kg_d loading rate file for responses
    print("Writing dummy zero and unit loading files")

    def _gen_rand_spike(arrshpe, spikefrac=0.25, seed=None):
        fltshp = np.multiply(*arrshpe)
        nspike = int(np.floor(spikefrac*fltshp))
        arr = np.zeros(fltshp)
        arr[:nspike] = 1
        if seed is not None:
            np.random.seed(seed)
        np.random.shuffle(arr)
        arr = arr.reshape(arrshpe)
        return arr

    for r in range(nloadreals):
        if seed_randreals:
            seed = r
        else:
            seed = None
            # np.random.seed(r)
        if enstype.startswith('dirac'):  # todo plus minus dirac
            perturb = spike_mag + (spike_mag * _gen_rand_spike(
                base_arr.shape, spikefrac=0.25, seed=seed))
        elif enstype.startswith('plusminus'):
            np.random.seed(seed)
            perturb = spike_mag * (2 * np.random.random(base_arr.shape) - 1)
        elif enstype.startswith('spikevar'):
            np.random.seed(seed)
            perturb = spike_mag + (spike_mag * np.random.random(base_arr.shape))
        elif enstype.startswith('spikerange'):
            np.random.seed(seed)
            perturb = spike_mag * (20 * np.random.random(base_arr.shape))
        else:  # basic
            perturb = spike_mag
        n_arr = o_arr+perturb
        np.savetxt(os.path.join(t_d, f"nload_{r+1}.dat"),  n_arr)

    # Need ins files for harvesting first run outputs
    # Base this on current ins - should just be able to read,replace,write
    print("Creating new instruction files and dummy output files:")
    b_d = os.getcwd()
    os.chdir(t_d)
    print("    For additional mt3d run")
    with open("mt3d001.ucn.dat.ins", 'r') as fin:
        with open("mt3d001.oucn.dat.ins", 'w') as fout:
            for line in fin.readlines():
                fout.write(line.replace('ucn', 'oucn'))
    # with open("MT3D001.UCN_timeseries.processed.ins", 'r') as fin:
    #     with open("MT3D001.oUCN_timeseries.processed.ins", 'w') as fout:
    #         for line in fin.readlines():
    #             fout.write(line.replace('gwnts', 'ogwnts'))
    # with open("hauraki_transient.mt3d.processed.ins", 'r') as fin:
    #     with open("hauraki_transient.omt3d.processed.ins", 'w') as fout:
    #         for line in fin.readlines():
    #             fout.write(line.replace('sfrc', 'osfrc'))
    shutil.copy("mt3d001.ucn.dat", "mt3d001.oucn.dat")
    # shutil.copy("MT3D001.UCN_timeseries.processed",
    #             "MT3D001.oUCN_timeseries.processed")
    # shutil.copy("hauraki_transient.mt3d.processed",
    #             "hauraki_transient.omt3d.processed")
    oucn_obs = pst.add_observations("mt3d001.oucn.dat.ins")
    # ogwnts_obs = pst.add_observations("MT3D001.oUCN_timeseries.processed.ins")
    # osft_obs = pst.add_observations("hauraki_transient.omt3d.processed.ins")
    obs = pst.observation_data
    obs.loc[oucn_obs.index, 'obgnme'] = 'oucn1'
    # obs.loc[ogwnts_obs.index, 'obgnme'] = "ogwnts"
    # obs.loc[osft_obs.index, 'obgnme'] = "osfrc"

    print("    And difference obs")
    # Prep for presence of difference obs
    with open("mt3d001.ucn.dat.ins", 'r') as fin:
        with open("mt3d001.ducn.dat.ins", 'w') as fout:
            for line in fin.readlines():
                fout.write(line.replace('ucn', 'ducn'))
    # with open("MT3D001.UCN_timeseries.processed.ins", 'r') as fin:
    #     with open("MT3D001.dUCN_timeseries.processed.ins", 'w') as fout:
    #         for line in fin.readlines():
    #             fout.write(line.replace('gwnts', 'dgwnts'))
    # with open("hauraki_transient.mt3d.processed.ins", 'r') as fin:
    #     with open("hauraki_transient.dmt3d.processed.ins", 'w') as fout:
    #         for line in fin.readlines():
    #             fout.write(line.replace('sfrc', 'dsfrc'))
    shutil.copy("mt3d001.ucn.dat", "mt3d001.ducn.dat")
    # shutil.copy("MT3D001.UCN_timeseries.processed",
    #             "MT3D001.dUCN_timeseries.processed")
    # shutil.copy("hauraki_transient.mt3d.processed",
    #             "hauraki_transient.dmt3d.processed")
    ducn_obs = pst.add_observations("mt3d001.ducn.dat.ins")
    # dgwnts_obs = pst.add_observations("MT3D001.dUCN_timeseries.processed.ins")
    # dsft_obs = pst.add_observations("hauraki_transient.dmt3d.processed.ins")
    obs = pst.observation_data
    obs.loc[ducn_obs.index, 'obgnme'] = 'ducn1'
    # obs.loc[dgwnts_obs.index, 'obgnme'] = "dgwnts"
    # obs.loc[dsft_obs.index, 'obgnme'] = "dsfrc"
    os.chdir(b_d)

    # 1. for run 1
    # 2. for run 2
    print("Modifying forward run to add extra"
          " mt3d run lines for with and without load runs")
    # Modify forward run
    # pull out difference_mtsim_outputs() function from this file
    func_lines = []
    abet_set = set(string.ascii_uppercase)
    abet_set.update(set(string.ascii_lowercase))
    with open('freyberg_sire_invest.py', 'r') as f:
        while True:
            line = f.readline()
            if line.startswith('def difference_mtsim_outputs'):
                func_lines.append(line)
                while True:
                    line = f.readline()
                    if line == '':
                        func_lines.append('\n')
                        break
                    if line[0] in abet_set:
                        func_lines.append('\n')
                        break
                    func_lines.append(line)
                break
    with open('freyberg_sire_invest.py', 'r') as f:
        while True:
            line = f.readline()
            if line.startswith('def extract_loadreal'):
                func_lines.append(line)
                while True:
                    line = f.readline()
                    if line == '':
                        func_lines.append('\n')
                        break
                    if line[0] in abet_set:
                        func_lines.append('\n')
                        break
                    func_lines.append(line)
                break

    with open(os.path.join(t_d, "forward_run.py"), 'r') as frun:
        lines = [line for line in frun.readlines()]
    lineout = ["import shutil\n"]
    redo_lines = []
    for line in lines:
        if line.startswith('def main'):
            lineout.extend(func_lines)
        if any([s in line for s in ["mt3d002", "MT3D002",
                                    "obs('hauraki_transient.mt3d",
                                    "hauraki_transient.hds",
                                    'hauraki_transient.list', 'sfr_obs']]):
            line = ''  # ' + line
            if any([s in lineout[-1] for s in ['try:', 'Exception']]):
                lineout[-1] = ''  # ' + lineout[-1]
        if 'load2crch' in line and 'def' not in line:
            # 1.1 Set dnz_processed_kg_d.dat to no load
            # 1.2 kg_load_helper.apply_kg_load()
            lineout.append(f"    loadreal = extract_loadreal({pe.index.shape[0]})\n")
            lineout.append("    shutil.copy("
                           "os.path.join('oload.dat'), "
                           "os.path.join('load.dat'))\n")
            # 2.2 kg_load_helper.apply_kg_load()
            redo_lines.append(line)
        if any([s in line for s in ["mt3dusgs", "obs('mt3d001.ucn",
                                    "timeseries('MT3D001.UCN"]]):
            # 1.3 run
            # 1.4 Apply pyemu.gw_utils.apply_hds_timeseries('MT3D001.UCN_timeseries.config',None)
            # 1.5 Apply pyemu.gw_utils.apply_sft_obs()
            redo_lines.append(line)
        lineout.append(line)
        if 'apply_hds_obs' in line:  # we at end of function setup second run
            redo_lines.append(line)
            # Copy results to new file
            # 1.6 Copy hauraki_transient.mt3d.processed to hauraki_transient.omt3d.processed
            # 1.7 Copy MT3D001.UCN_timeseries.processed to MT3D001.oUCN_timeseries.processed
            # 1.8 Copy mt3d001.ucn.dat to mt3d001.oucn.dat
            lineout.extend([
                "    shutil.copy('mt3d001.ucn.dat', 'mt3d001.oucn.dat')\n"])
            # Copy load for next run
            # 2.1 Set dnz_processed_kg_d.dat to load
            lineout.append(
                "    shutil.copy(os.path.join(f'nload_{loadreal}.dat'), "
                "os.path.join('load.dat'))\n")
            # 2.2 kg_load_helper.apply_kg_load()
            # 2.3 run
            # 2.4 Apply pyemu.gw_utils.apply_hds_timeseries('MT3D001.UCN_timeseries.config',None)
            # 2.5 Apply pyemu.gw_utils.apply_sft_obs()
            lineout.extend(redo_lines)
            # 2.6 Run differencing function (difference_mtsim_outputs())
            lineout.append("    difference_mtsim_outputs()\n")
    with open(os.path.join(t_d, "forward_run_respunc.py"), 'w') as frun:
        for line in lineout:
            frun.write(line)
    pst.model_command = 'python forward_run_respunc.py'

    print("Writing .pst")
    exp_pe = pyemu.ParameterEnsemble(pst, exp_pe)
    exp_pe.to_binary(os.path.join(t_d, 'par.jcb'))
    pst.pestpp_options['sweep_parameter_csv_file'] = "par.jcb"
    pst.pestpp_options['sweep_output_csv_file'] = "respunc.csv"  # sweep can only write csv
    pst.pestpp_options['sweep_chunk'] = 500
    pst.control_data.noptmax = -1
    pst.write(os.path.join(t_d, "freyberg.pst"))


def prep_for_condor(m_d, t_d="template_transient",
                    flow_only=False, pestpp_exe="ipestpp-ies.exe",
                    bin_dir=os.path.join("C:\\","bin"),
                    ct_d="template_condor", stomp=False, nreal=30):
    if os.path.exists(ct_d):
        shutil.rmtree(ct_d)
    shutil.copytree(
        t_d, ct_d,
        ignore=shutil.ignore_patterns("*.list","*.jcb","*.out","*.hds","*.rec",
                                      "*.mt3d","*.ucn","*.rei","*.rns",
                                      "*.rnj", "*_setup_*"))
    # need to send ftl if not running modflow - as is case for load response mat
    rm_ext_list = [".list",".jcb",".out",".hds",".rec",".mt3d",".ucn",".rei",
                   ".rns",".rnj"]
    rm_tag_list = ["_setup_"]
    for rm_ext in rm_ext_list:
        rm_files = [f for f in os.listdir(ct_d) if f.lower().endswith(rm_ext)]
        [os.remove(os.path.join(ct_d,rm_file)) for rm_file in rm_files]

    for rm_tag in rm_tag_list:
        rm_files = [f for f in os.listdir(ct_d) if rm_tag in f.lower()]
        [os.remove(os.path.join(ct_d,rm_file)) for rm_file in rm_files]

    a_d = os.path.join(ct_d,"mult")
    [os.remove(os.path.join(a_d,f)) for f in os.listdir(a_d)[1:]]

    exe_list = ["mfnwt.exe", "mt3dusgs.exe", pestpp_exe]
    # bin_dir = os.path.join("C:\\","bin")

    for exe in exe_list:
        shutil.copy2(os.path.join(bin_dir, exe), os.path.join(ct_d,exe))

    dev_dir = os.path.join('..','..')  # os.path.join("C:\\","Dev")
    paks = ["pyemu", "flopy"]
    for pak in paks:
        shutil.copytree(os.path.join(dev_dir, pak, pak),
                        os.path.join(ct_d, pak))

    if stomp and m_d is not None:
        if os.path.exists(m_d):
            shutil.rmtree(m_d)
    else:
        assert not os.path.exists(m_d), f"master dir already exists {m_d}"
    shutil.copytree(t_d, m_d)
    for pak in paks:
        shutil.copytree(os.path.join(dev_dir, pak, pak),
                        os.path.join(m_d, pak))
    for exe in exe_list:
        shutil.copy2(os.path.join(bin_dir, exe), os.path.join(m_d, exe))
    pst = pyemu.Pst(os.path.join(m_d,"freyberg.pst"))

    if flow_only:
        obs = pst.observation_data
        obs.loc[obs.obgnme.apply(lambda x: "gwnts" in x or
                                           "swnts" in x or
                                           "tr_" in x),"weight"] = 0
    if "obs.jcb" in os.listdir(m_d):
        print("found obs.jcb, using for ies_obs_en")
        pst.pestpp_options["ies_obs_en"] = "obs.jcb"
    pst.pestpp_options["ies_par_en"] = "par.jcb"
    pst.pestpp_options["ies_num_reals"] = nreal
    pst.pestpp_options["ies_save_binary"] = True
    pst.pestpp_options["ies_initial_lambda"] = 1000.0
    pst.write(os.path.join(m_d, "freyberg.pst"))


def run_and_process(m_d, pstname='freyberg.pst',
                    port=4004, run=True, process=True, it=6,
                    pestpp_exe="ipestpp-ies.exe", d4condor=None, queue=100):
    if run:
        # modify condor submission script with specific args
        with open("sub_template.sub", 'r') as ft:
            with open(f"sub_{port}.sub", 'w') as fs:
                for line in ft:
                    if 'arguments' in line:
                        temp = line.split()
                        p_idx = [i for i, t in enumerate(temp)
                                 if 'pestpp' in t][0]
                        temp[p_idx] = pestpp_exe
                        p_idx = [i for i, t in enumerate(temp) if '.pst' in t][
                            0]
                        temp[p_idx] = pstname
                        p_idx = [i for i, t in enumerate(temp) if t == '4004'][
                            0]
                        temp[p_idx] = str(port)
                        line = f"{' '.join(temp)}\n"
                    if d4condor is not None:
                        if 'template_condor' in line:
                            line = line.replace('template_condor', d4condor)
                    if 'queue' in line:
                        line = line.replace('100', str(queue))
                    fs.write(line)
        os.system("condor_submit sub_{0}.sub".format(port))
        pyemu.os_utils.run(f"{pestpp_exe} {pstname} /h :{port}", cwd=m_d)
    if process:
        if 'respunc' in m_d:
            sire.process_sweep_csv(pstname, m_d,
                                   csvroot='respunc', split_dict=None,
                                   overwrite=False)
        elif 'resp' in m_d:
            pst = pyemu.Pst(os.path.join(m_d, pstname))
            obs = pst.observation_data
            obscols = {c: obs.loc[obs.obsnme.str.contains(c), 'obsnme'].tolist()
                       for c in [f'ucn_{i:02d}' for i in range(3)]}
            sire.process_sweep_csv(pst, m_d, split_dict=obscols,
                                   overwrite=False)


def test_process_response_unc_res(resp_d='master_respunc', load_all_csv=True,
                                  pcts=None, cdfplots=True):
    import matplotlib.animation as animation
    import matplotlib.cm as cm
    import matplotlib.colors as colors

    def init_ani():
        im.set_data(np.zeros([mt.nrow, mt.ncol]))
        tsim = None
        return im

    def update_ani(frame, ars, k, ts, cvmax, svmax, yrs):
        t = ts[frame]
        p = ars[frame]
        year = yrs[frame]
        # if ints is not None:
        #     tin = ints.loc[year - 1]
        #     tsim.set_offsets(np.array([year - 1, tin]))
        #     tsim.set_array(np.array([tin]))
        print(f'adding concentration '
              f'for layer {k} at time {t}')
        # res_plot._add_hatch(inact[k])
        im.set_data(p)
        if sfc and k == 0:
            sfc_ar = sar[frame]
            ax.imshow(sfc_ar, alpha=1.0, vmin=0., vmax=maxc,
                                    cmap='cool', animated=True)
        ax.set_title(
            f'Layer {k + 1} after {t / 365.:.1f} yrs @ '
            f'{year}')
        # plt_name = (f'{mt.name}_resp_{sp_cell}_'
        #             f'{k + 1}_{int(round(t)):d}')
        # fig.savefig(
        #     os.path.join(savespace, f'{plt_name}.pdf'),
        #     bbox_inches='tight', dpi=200)
        # tim.remove()
        return im

    writer = animation.FFMpegWriter(fps=2, metadata=dict(artist='Me'),
                                    bitrate=1800)

    savespace = os.path.join(resp_d, 'plots')
    if not os.path.exists(savespace):
        os.mkdir(savespace)
    print("Reading pst")
    pst = pyemu.Pst(os.path.join(resp_d, "freyberg.pst"))
    obs = pst.observation_data
    print("Read Sims")
    mm = flopy.modflow.Modflow.load("freyberg.nam", model_ws=resp_d,
                                    check=False, verbose=False,
                                    load_only=['BAS6', 'DIS', 'SFR'])
    mt = flopy.mt3d.Mt3dms.load("freyberg_mt.nam", model_ws=resp_d,
                                modflowmodel=mm)
    all_times = np.cumsum(mt.btn.perlen.array)
    if load_all_csv:
        sire.process_sweep_csv(pst, resp_d, csvroot='respunc', split_dict=None,
                               overwrite=True)
    # if load_all_csv or not os.path.exists(
    #         os.path.join(resp_d, f'respunc.jcb')):
    #     print("Loading output 'ensemble' CSV")
    #     print(f"    Loading output 'ensemble'")
    #     df = pd.read_csv(os.path.join(resp_d, 'respunc.csv'), low_memory=False)
    #     # jcb reals (realisation number not nec. preserved)
    #     print("    Writing run id info")
    #     df.input_run_id.to_csv(os.path.join(resp_d, "runid.csv"),
    #                            header=True, index=True)
    #     print(f"    Converting oe to jcb")
    #     resp_en = pyemu.ObservationEnsemble(pst, df.drop('input_run_id', axis=1))
    #     resp_en.to_binary(os.path.join(resp_d, f'respunc.jcb'))
    oe = pyemu.ObservationEnsemble.from_binary(
        pst, os.path.join(resp_d, 'respunc.jcb'))._df.T.loc[obs.index]
    # realids = pd.read_csv(os.path.join(resp_d, 'runid.csv'), index_col=0)
    reachdata = pd.DataFrame(mm.sfr.reach_data).set_index('reachID')
    stdt = pd.to_datetime(mm.modeltime.start_datetime, format='%Y-%m-%d %H')
    obs = sire._parse_kijt_resp_obs(obs, stdt, all_times, reachdata)
    oe[['k', 'i', 'j', 't']] = obs.loc[
        oe.index, ['k', 'i', 'j', 't']].astype(int)
    print("Extracting Conc diff obs")
    cgw = oe.loc[obs.obsnme.str.startswith('ucn')].copy()
    dcgw = oe.loc[obs.obsnme.str.startswith('ducn')].copy()

    kper = dcgw.t.unique()
    dcgw['redt'] = kper.searchsorted(dcgw.t)
    dcgw['date'] = pd.to_datetime(
        mm.modeltime.start_datetime,
        format='%Y-%m-%d %H') + pd.to_timedelta(all_times[dcgw.t], 'D')
    cgw['redt'] = kper.searchsorted(cgw.t)
    cgw['date'] = pd.to_datetime(
        mm.modeltime.start_datetime,
        format='%Y-%m-%d %H') + pd.to_timedelta(all_times[cgw.t], 'D')

    dates = dcgw.date.dt.strftime('%Y-%m-%d').unique()
    cgw = cgw.set_index(['k', 'i', 'j', 't', 'redt', 'date'])
    cgw[cgw.abs() > 1e10] = np.nan
    dcgw = dcgw.set_index(['k', 'i', 'j', 't', 'redt', 'date'])
    # dcgw = dcgw.mask(dcgw.abs() < 1e-12)  # TODO implications of masking small vals...
    # *** de-mean ensemble
    cdemean = cgw.divide(cgw.mean(axis=1), axis=0)
    cdemean = cdemean.replace([np.inf, -np.inf], np.nan)  # deal with case where mean 0 but small values exist
    creduced = cgw.subtract(cgw.mean(axis=1), axis=0)

    demean = dcgw.divide(dcgw.mean(axis=1), axis=0)
    demean = demean.replace([np.inf, -np.inf], np.nan)  # deal with case where mean 0 but small values exist
    reduced = dcgw.subtract(dcgw.mean(axis=1), axis=0)

    hdfnme = os.path.join(resp_d, 'conc_unc.hdf')
    print(f"Writing conc hdf to {hdfnme}")
    cgw.to_hdf(hdfnme, 'df', mode='w')
    hdfnme = os.path.join(resp_d, 'conc_dm_unc.hdf')
    print(f"Writing normed conc hdf to {hdfnme}")
    cdemean.to_hdf(hdfnme, 'df', mode='w')
    hdfnme = os.path.join(resp_d, 'conc_shift_unc.hdf')
    print(f"Writing shifted conc hdf to {hdfnme}")
    creduced.to_hdf(hdfnme, 'df', mode='w')


    hdfnme = os.path.join(resp_d, 'change_unc.hdf')
    print(f"Writing change hdf to {hdfnme}")
    dcgw.to_hdf(hdfnme, 'df', mode='w')
    hdfnme = os.path.join(resp_d, 'demean_unc.hdf')
    print(f"Writing de-meaned hdf to {hdfnme}")
    demean.to_hdf(hdfnme, 'df', mode='w')
    hdfnme = os.path.join(resp_d, 'shift_unc.hdf')
    print(f"Writing shifted hdf to {hdfnme}")
    reduced.to_hdf(hdfnme, 'df', mode='w')

    if cdfplots:
        en_dict={'change': dcgw, 'demean': demean, 'reduced':reduced}
        print("Plotting de-meaned cell/cell CDFs")
        # Some test plots
        for lay in range(mm.nlay):
            print(f"    Layer {lay+1}")
            for k, en in en_dict.items():
                fig, axs = plt.subplots(2, 1, figsize=(8, 11))
                ax = axs[0]
                # if k in {'demean', 'change'}:
                    # ax.set_xscale('log')
                for _, s in en.loc[(lay, slice(None), slice(None), slice(None),
                                        0, slice(None)), :].dropna(0, 'all').iterrows():
                    # if k in {'demean', 'change'}:
                    #     b = np.logspace(np.log10(s[s > 0].min()),
                    #                     np.log10(s.max()), 100)
                    # else:
                    if (s == 0.).all():
                        ax.plot([0, 0], [0, 1])
                    else:
                        b = 100
                        n, bins, patches = ax.hist(s, b, density=True, histtype='step',
                                                   cumulative=True)
                        patches[0].set_xy(patches[0].get_xy()[:-1])
                ax.set_xlabel(f'{k} $\Delta$Conc')

                ax = axs[1]
                # if k in {'demean', 'change'}:
                #     ax.set_xscale('log')
                for _, s in en.loc[(lay, slice(None), slice(None), kper[-1],
                                        slice(None), slice(None)), :].dropna(0, 'all').iterrows():
                    # if k in {'demean', 'change'}:
                    #     b = np.logspace(np.log10(s[s > 0].min()),
                    #                     np.log10(s.max()), 100)
                    # else:
                    if (s == 0.).all():
                        ax.plot([0, 0], [0, 1])
                    else:
                        b = 100
                        n, bins, patches = ax.hist(s, b, density=True, histtype='step',
                                                   cumulative=True)
                        patches[0].set_xy(patches[0].get_xy()[:-1])
                ax.set_xlabel(f'{k} $\Delta$Conc')
                fig.tight_layout()
                fig.savefig(
                    os.path.join(savespace, f"Lay{lay}_{k}_deltaconc_unc.pdf"),
                    dpi=200)

    if pcts is not None:
        print("Generating timeseries movie files")
        # Test timeseries plots through de meaned distibutions
        # set up container for e.g. array
        shp4d = [len(kper), mm.nlay, mm.nrow, mm.ncol]
        mask = np.broadcast_to(mt.btn.icbund.array== 0, shp4d).copy()
        # base = realids.index[realids.input_run_id=='BASE'].astype(str)[0]
        k = 0
        sfc = False
        for pct in pcts:
            print(f"    Movie for quantile {pct}")
            # *** get percentile value for each entry in row
            # pct_rank = dcgw.rank(method='max', pct=True, axis=1)
            # pctile = demean.lookup(
            #     pct_rank.index, pct_rank.sub(pct).mask(pct_rank.sub(pct) < 0).idxmin(
            #         axis=1).values)
            # or extract percentile
            print(f"     Extracting quantile {pct}")
            pctile = demean.quantile(pct, axis=1, interpolation='linear')
            ar = np.zeros(shp4d)
            ar = np.ma.masked_array(ar, mask)
            ar[tuple(pctile.reset_index()[
                         ['redt','k', 'i', 'j']].values.T)] = pctile.values
            plar = ar[:, k, :, :]
            minc = np.nanmin(plar)
            maxc = np.nanmax(plar)
            smp = cm.ScalarMappable(
                norm=colors.Normalize(minc, maxc),
                cmap=cm.plasma)
            smp.set_array([])
            fig, ax = plt.subplots(figsize=(10, 15))
            cbar = fig.colorbar(smp)
            cbar.set_label('$\Delta$GW\nconc (kgm$^{-3}$)')
            im = ax.imshow(np.zeros([mt.nrow, mt.ncol]),
                           animated=True, vmin=minc, vmax=maxc, cmap='plasma')
            ani = animation.FuncAnimation(
                fig, update_ani, len(kper),
                fargs=[plar, k, all_times[kper], maxc, 1, dates],
                init_func=init_ani, blit=False, repeat=True)
            ani.save(
                os.path.join(savespace,
                             f'{mt.name}_baseresp_'
                             f'{k + 1}_{str(pct)}.mp4'),
                writer=writer)

    return dcgw, demean, reduced


def invest_unc_scaling(run=True, rd='basic', uniformbase=True):
    from multiprocessing import Process, Lock
    lock = Lock()
    t_d = f'template_{rd}_respunc'
    m_d = f'master_{rd}_respunc'
    ctd = f'template_condor_{rd}'
    spms = [1, 10]
    jobs = []
    for spm in spms:
        if run:
            prep_for_response_ensemble_unc(t_d=f'{t_d}{spm}',
                                           spike_mag=spm, enstype=rd,
                                           uniformbase=uniformbase)
            prep_for_condor(f'{m_d}{spm}', f'{t_d}{spm}',
                            pestpp_exe="pestpp-swp.exe",
                            bin_dir=os.path.join('..', 'exe'),
                            ct_d=f'{ctd}{spm}', stomp=True)
        print(f"Starting {m_d}{spm} with run={run}")
        p = Process(target=run_and_process,
                    args=(f'{m_d}{spm}', 'freyberg.pst', 4006+spm, run,
                          True, 6, "pestpp-swp.exe", f'{ctd}{spm}'))
        lock.acquire()
        p.start()
        lock.release()
        jobs.append(p)

    for job in jobs:
        job.join()
    ch1, dm1, rd1 = test_process_response_unc_res(f'{m_d}{spms[0]}',
                                                  load_all_csv=False,
                                                  pcts=None, cdfplots=True)
    ch2, dm2, rd2 = test_process_response_unc_res(f'{m_d}{spms[1]}',
                                                  load_all_csv=False,
                                                  pcts=None, cdfplots=True)
    fig, ax = plt.subplots(figsize=(8, 8))
    ax.hist(ch1.iloc[500], bins=100, histtype='step')
    ax.hist(ch2.iloc[500], bins=100, histtype='step')
    # ax.set_xscale('log')
    fig.savefig(os.path.join(f'{m_d}{spms[0]}', "cell1300_change_unc.png"))


def basic(r_d='.', t_d=f'template_basic', run=True, build_respmat=True,
          uniformbase=True, nreals=30):
    """Ensemble of response matricies"""
    btd = t_d
    # Response Matrix
    m_d = 'master_basic_resp'
    it_d = os.path.join(r_d, 'new_temp')
    t_d = btd +"_resp"
    if build_respmat:
        if run:
            if os.path.exists(t_d):
                shutil.rmtree(t_d)
            print(f"Copying PEST template dir to {t_d}")
            shutil.copytree(it_d, t_d)
            print("Loading par ensembles")
            pstfile = 'freyberg.pst'
            pst = pyemu.Pst(os.path.join(t_d, pstfile))
            pe = pyemu.ParameterEnsemble.from_binary(pst, os.path.join(t_d, f'par.jcb'))
            print("Adding dummy base load file for response runs")
            # Dummy kg_d loading rate file for responses
            base_arr = np.loadtxt(os.path.join(t_d, "org", "load.dat"))
            np.savetxt(os.path.join(t_d, "org", "load.dat.orig"), base_arr)
            dum_arr = np.ones(base_arr.shape)*10
            np.savetxt(os.path.join(t_d, "org", "load.dat"), dum_arr)
            print("Setting initial concentrations to 0")
            # initial conc = 0
            sconcf = [f for f in os.listdir(t_d) if 'sconc1' in f]
            ar = np.loadtxt(os.path.join(t_d, sconcf[0]))
            zs = np.zeros(ar.shape)
            for f in sconcf:
                np.savetxt(os.path.join(t_d, "org", f), zs, fmt='%15.6E', delimiter='')

            print("Setting up load pars for response matrix run")
            pars = pst.parameter_data
            # grid load pars to 0, set derinc, etc and un-fix
            load_pars = pars.parnme.str.startswith('multiplier_load')
            pars.loc[load_pars, 'partrans'] = 'none'
            pars.loc[load_pars, 'parval1'] = 0
            pst.parameter_data = pars
            pargp = pst.parameter_groups
            pargp.loc["load", "derinc"] = 1.0
            pargp.loc["load", "inctyp"] = "absolute"
            pst.parameter_groups = pargp

            # For response matrix run need to build dummy ensemble of containing
            # cell-by-cel spiking
            load_pars = pars.loc[load_pars].index
            resp_par_en = pd.DataFrame(np.zeros([len(load_pars), pst.npar]),
                                       index=range(len(load_pars)),
                                       columns=pars.index)
            resp_par_en.loc[:, pars.index] = pars.parval1.values
            resp_par_en.loc[:, load_pars] = np.identity(len(load_pars))
            # append base run
            resp_par_en = resp_par_en.append(pars.parval1.rename('base'))
            ren = pyemu.ParameterEnsemble(pst, resp_par_en)
            ren.to_binary(os.path.join(t_d, "resp_par.jcb"))
            # need to store real number against spike par
            spike_rel = pd.Series(dict(resp_par_en.loc[:, load_pars].replace(
                0.0, np.nan).stack().index.values))
            spike_rel.to_csv(os.path.join(t_d, "resp_reals.csv"), header=False)

            print("Writing .pst")
            pst.pestpp_options['sweep_parameter_csv_file'] = "resp_par.jcb"
            pst.pestpp_options['sweep_output_csv_file'] = "responses.csv"  # sweep can only write csv
            pst.pestpp_options['sweep_chunk'] = 500
            pst.control_data.noptmax = -1
            pst.write(os.path.join(t_d, "freyberg.pst"))

            prep_for_condor(m_d, t_d,
                            pestpp_exe="pestpp-swp.exe",
                            bin_dir=os.path.join('..', 'exe'),
                            stomp=True, nreal=nreals)

        run_and_process(m_d, port=4004, run=run, process=True,
                        pestpp_exe="pestpp-swp.exe")
        resp_matrix_from_oe(m_d)

    # Ensemble UNC
    invest_unc_scaling(run=run, rd='basic', uniformbase=uniformbase)


def benchmark(r_d='.', t_d=f'template_bmk', run=True, nreals=30,
              low_memory=False, uniformbase=True):
    """Ensemble of response matricies"""
    btd = t_d
    # Response Matrix
    m_d = 'master_bmk_resp'
    it_d = os.path.join(r_d, 'new_temp')
    t_d = btd +"_resp"
    if run:
        if os.path.exists(t_d):
            shutil.rmtree(t_d)
        print(f"Copying PEST template dir to {t_d}")
        shutil.copytree(it_d, t_d)
        print("Loading par ensembles")
        pstfile = 'freyberg.pst'
        pst = pyemu.Pst(os.path.join(t_d, pstfile))
        pe = pyemu.ParameterEnsemble.from_binary(
            pst, os.path.join(t_d, f'par.jcb'))._df
        pe = pe.iloc[range(nreals), :]

        print("Adding dummy base load file for response runs")
        # Dummy kg_d loading rate file for responses
        base_arr = np.loadtxt(os.path.join(t_d, "org", "load.dat"))
        np.savetxt(os.path.join(t_d, "org", "load.dat.orig"), base_arr)
        dum_arr = np.ones(base_arr.shape)*10
        np.savetxt(os.path.join(t_d, "org", "load.dat"), dum_arr)
        print("Setting initial concentrations to 0")
        # initial conc = 0
        sconcf = [f for f in os.listdir(t_d) if 'sconc1' in f]
        ar = np.loadtxt(os.path.join(t_d, sconcf[0]))
        zs = np.zeros(ar.shape)
        for f in sconcf:
            np.savetxt(os.path.join(t_d, "org", f), zs, fmt='%15.6E', delimiter='')

        print("Setting up load pars for response matrix run")
        pars = pst.parameter_data
        # grid load pars to 0, set derinc, etc and un-fix
        load_pars = pars.parnme.str.startswith('multiplier_load')
        pars.loc[load_pars, 'partrans'] = 'none'
        pars.loc[load_pars, 'parval1'] = 0
        pst.parameter_data = pars
        pargp = pst.parameter_groups
        pargp.loc["load", "derinc"] = 1.0
        pargp.loc["load", "inctyp"] = "absolute"
        pst.parameter_groups = pargp

        # For response matrix run need to build dummy ensemble of containing
        # cell-by-cel spiking
        load_pars = pars.loc[load_pars].index
        # set load pars to initally zero
        pe.loc[:, load_pars] = 0.
        nest_pe_len = len(load_pars)*len(pe.index)
        # create load spike array
        if low_memory:
            block0 = np.identity(len(load_pars))
            spike_rel = []
            tmp_pe = pd.DataFrame(index=range(len(load_pars)),
                                  columns=pe.columns)
            idxs = pd.Index(np.arange(len(load_pars)) * len(pe.index))
            with open(os.path.join(t_d, 'resp_par.csv'), 'w', newline='') as fp:
                for i in range(len(pe.index)):
                    print(f'Writing par ensemble for sweep {i+1}/{len(pe.index)}')
                    tmp_pe.loc[:, pe.columns] = pe.iloc[i, :].values
                    tmp_pe.loc[:, load_pars] = block0
                    if i == 0:
                        header = True
                    else:
                        header = False
                    idx = idxs + i
                    tmp_pe = tmp_pe.set_index(idx).rename_axis('real_name').astype(np.float32)
                    tmp_pe = tmp_pe.astype({c: np.int16 for c in load_pars})
                    tmp_pe.to_csv(fp, mode='a', header=header, compression='gzip')
                    spike_rel.append(
                        pd.Series(dict(tmp_pe.loc[:, load_pars].replace(
                            0.0, np.nan).stack().index)))
                pe.astype(np.float32).set_index(
                    'base' + pe.reset_index(drop=True).index.astype(
                        str)).to_csv(
                    fp, mode='a', header=header)
            spike_rel = pd.concat(spike_rel)
            pst.pestpp_options['sweep_parameter_csv_file'] = "resp_par.csv"
        else:
            block0 = np.zeros((len(pe.index), len(load_pars)))
            block0[:, 0] = 1
            load_arr = np.concatenate([np.roll(block0, i)
                                       for i in range(len(load_pars))], axis=0)
            resp_par_en = pd.DataFrame(np.zeros([nest_pe_len, pst.npar]),
                                       index=range(nest_pe_len),
                                       columns=pars.index)
            resp_par_en.loc[:, pars.index] = np.tile(
                pe.loc[:, pars.index].values, (len(load_pars), 1))
            resp_par_en.loc[:, load_pars] = load_arr
            # append base runs
            resp_par_en = resp_par_en.append(
                pe.set_index('base'+pe.index.astype(str)))
            ren = pyemu.ParameterEnsemble(pst, resp_par_en)
            ren.to_binary(os.path.join(t_d, "resp_par.jcb"))
            pst.pestpp_options['sweep_parameter_csv_file'] = "resp_par.jcb"
            # need to store real number against spike par
            spike_rel = pd.Series(dict(resp_par_en.loc[:, load_pars].replace(
                0.0, np.nan).stack().index.values))
        spike_rel.to_csv(os.path.join(t_d, "resp_reals.csv"), header=False)

        print("Writing .pst")
        pst.pestpp_options['sweep_output_csv_file'] = "responses.csv"  # sweep can only write csv
        pst.pestpp_options['sweep_chunk'] = 500
        pst.control_data.noptmax = -1
        pst.write(os.path.join(t_d, "freyberg.pst"))

        prep_for_condor(m_d, t_d,
                        pestpp_exe="pestpp-swp.exe",
                        bin_dir=os.path.join('..', 'exe'), stomp=True,
                        nreal=nreals)

    run_and_process(m_d, port=4005, run=run, process=True,
                    pestpp_exe="pestpp-swp.exe", queue=300)
    resp_matrix_from_oe(m_d)

    # Ensemble UNC
    # invest_unc_scaling(run=run, rd='bmk')


def lo_benchmark(r_d='.', t_d=f'template_lobmk', run=True, nreals=30,
                 low_memory=False, uniformbase=True, loworder=9,
                 globalrun=False):
    """Ensemble of response matricies"""
    import cProfile
    import multiprocessing as mp
    import pathlib
    btd = t_d
    # Response Matrix
    it_d = os.path.join(r_d, 'new_temp')
    t_d = btd +"_resp"
    m_d = t_d.replace('template', 'master')
    if run:
        if os.path.exists(t_d):
            shutil.rmtree(t_d)
        print(f"Copying PEST template dir to {t_d}")
        shutil.copytree(it_d, t_d)
        print("Loading par ensembles")
        pstfile = 'freyberg.pst'
        pst = pyemu.Pst(os.path.join(t_d, pstfile))
        pe = pyemu.ParameterEnsemble.from_binary(
            pst, os.path.join(t_d, f'par.jcb'))._df
        pe = pe.iloc[range(nreals), :]

        print("Adding dummy base load file for response runs")
        # Dummy kg_d loading rate file for responses
        base_arr = np.loadtxt(os.path.join(t_d, "org", "load.dat"))
        nrow, ncol = base_arr.shape
        np.savetxt(os.path.join(t_d, "org", "load.dat.orig"), base_arr)
        dum_arr = np.ones(base_arr.shape)*10
        np.savetxt(os.path.join(t_d, "org", "load.dat"), dum_arr)
        # print("Setting initial concentrations to 0")
        # # initial conc = 0
        # sconcf = [f for f in os.listdir(t_d) if 'sconc1' in f]
        # ar = np.loadtxt(os.path.join(t_d, sconcf[0]))
        # zs = np.zeros(ar.shape)
        # for f in sconcf:
        #     np.savetxt(os.path.join(t_d, "org", f), zs, fmt='%15.6E', delimiter='')

        print("Setting up load pars for response matrix run")
        pars = pst.parameter_data
        # grid load pars to 0, set derinc, etc and un-fix
        load_pars = pars.parnme.str.startswith('multiplier_load')
        pars.loc[load_pars, 'partrans'] = 'none'
        pars.loc[load_pars, 'parval1'] = 0
        pst.parameter_data = pars
        pargp = pst.parameter_groups
        pargp.loc["load", "derinc"] = 1.0
        pargp.loc["load", "inctyp"] = "absolute"
        pst.parameter_groups = pargp

        # For response matrix run need to build dummy ensemble of containing
        # cell-by-cel spiking
        # load_pars = pars.loc[load_pars].index
        # for low order nested ensemble version
        # the order of load pars could be critical
        # need to be by row and column
        load_pars = pars.loc[
            load_pars, ['i', 'j']
        ].astype(int).sort_values(['i', 'j'])  # can work out which ensemble members to run from i and j

        # set load pars to initally zero
        pe.loc[:, load_pars.index] = 0.
        if loworder is not None:
            assert np.remainder(np.sqrt(loworder), 1) == 0, (
                f"if loworder is not None, must be a square number, "
                f"{loworder} passed"
            )
            # number of inner ensemble members per spike cell
            ien_len = (nreals-1)//loworder
            # save one run for a global run (run realisation for each spike
            # number of en members that will be run for all spikes
            # (or optionally not run at all)
            gien_len = np.remainder((nreals-1), loworder)
            # Think good to run these for each spike as give means of
            # scaling mapped ensembles members for other spike cells runs
            # Actually now looking at running member number 0 as global
            # (i.e. for each spike)
        else:
            ien_len = nreals
        # total number of reals with nested ensemble
        # create load spike array
        # need to align lower order nested par ensemble with a loading array
        # some wizardry needed here to make sure the right ensemble
        # member is repeated for the right spike realisation
        if globalrun:
            g = 1  # plus 1 for global 0 run
            realorder = np.concatenate(
                load_pars.apply( # noqa - concat to call existing pe in correct order
                lambda x: np.concatenate(
                    [[0], np.arange(1, ien_len + 1) +  # noqa - for range or reals, use real 0 for global run
                          np.mod(x.j, loworder ** 0.5) * ien_len +  # noqa - shift start point for each column
                          np.mod(x.i, loworder ** 0.5) * ien_len * loworder ** 0.5]), # noqa - shift start point for each row
                axis=1).values).astype(int).astype(str)  # noqa - original pe as int strings
        else:
            g = 0
            realorder = np.concatenate(load_pars.apply(# noqa - concat to call existing pe in correct order
                lambda x: np.arange(0, ien_len) +  # noqa - for range or reals
                          np.mod(x.j, loworder ** 0.5) * ien_len +  # noqa - shift start point for each column
                          np.mod(x.i, loworder ** 0.5) * ien_len * loworder ** 0.5, # noqa - shift start point for each row
                axis=1).values).astype(int).astype(str)  # noqa - original pe as int strings
        nest_pe_len = (g + ien_len) * len(load_pars)
        block0 = np.zeros(
            [ien_len + g, len(load_pars)])  # plus for global 0 run
        block0[:, 0] = 1  # spike first cell for each ensemble member
        load_arr = np.concatenate([np.roll(block0, i)
                                   for i in range(len(load_pars))], axis=0)
        if low_memory:
            # pr = cProfile.Profile()
            # pr.enable()
            chunk_len = 5
            num_chunk_floor = len(pe.index) // chunk_len
            main_chunks = np.arange(len(pe.index))[: num_chunk_floor * chunk_len].reshape([-1, chunk_len]).tolist()
            remainder = np.arange(len(pe.index))[num_chunk_floor * chunk_len :].tolist()
            chunks = main_chunks + remainder
            manager = mp.Manager()
            ns = manager.Namespace()
            spike_rel = manager.dict()
            out_rel = manager.dict()
            fmt = f"0{len(str(len(pe.index)))}d"
            # spike_rel = {}
            # out_rel = {}
            with mp.get_context("spawn").Pool() as pool:
                for c in chunks:
                    ou = [
                        pool.apply_async(
                            _build_and_write_pe,
                            args=(c, pe, load_arr, load_pars, realorder,
                                  spike_rel, out_rel, t_d)
                        )
                    ]
                [r.get() for r in ou]
                print("closing pool")
                pool.close()

                print("joining pool")
                pool.join()

                fp = os.path.join(t_d, f'resp_par{len(pe.index):{fmt}}.csv')
                print(f'Writing BASE par ensemble for sweep ')
                pe.astype(np.float32).set_index(
                    'base'+pe.index.astype(str)).to_csv(fp, header=False)

            pst.pestpp_options['sweep_parameter_csv_file'] = "resp_par.csv"
            spike_rel = pd.DataFrame.from_dict(
                spike_rel, orient='index', columns=['parnme'])
            spike_rel['outer_realid'] = pd.DataFrame.from_dict(
                out_rel, orient='index', columns=['outer_realid'])
            s = f"{'?':?<{len(str(len(pe.index)))}}"
            files = sorted(
                [x.name for x in pathlib.Path(t_d).glob(f"resp_par{s}.csv")]
            )
            subprocess.run(["copy", "+".join(files), "resp_par.csv"], cwd=t_d,
                           capture_output=True, shell=True)
            # pr.disable()
        else:
            # creating spiking componenet of total nested pe
            # e.g. if 3 ensemble members are run for each spike
            # (ien_len=nreal//loworder=3):
            #      [[1,0,0,0,0,0,0,0,0,0,0,0],
            #       [1,0,0,0,0,0,0,0,0,0,0,0],
            #       [1,0,0,0,0,0,0,0,0,0,0,0],
            #       [0,1,0,0,0,0,0,0,0,0,0,0],
            #       [0,1,0,0,0,0,0,0,0,0,0,0],
            #       [0,1,0,0,0,0,0,0,0,0,0,0],...]
            resp_par_en = pd.DataFrame(np.zeros([nest_pe_len, pst.npar]),
                                       index=range(nest_pe_len),
                                       columns=pars.index)
            # generate array that describes the repetition of realisations
            # relative to the order of spiking (along rows)
            resp_par_en.loc[:, pars.index] = pe.loc[realorder, pars.index].values
            resp_par_en.loc[:, load_pars.index] = load_arr
            # append global run now already wrapped in - easier for post processing
            # blockg = np.identity(len(load_pars))
            # tmp_pe = pd.DataFrame(index=range(len(load_pars)),
            #                       columns=pe.columns)
            # tmp_pe.loc[:, pe.columns] = pe.iloc[0, :].values
            # tmp_pe.loc[:, load_pars.index] = blockg
            # resp_par_en = resp_par_en.append(tmp_pe, ignore_index=True)
            # Append base runs
            resp_par_en = resp_par_en.append(
                pe.set_index('base'+pe.index.astype(str)))
            # back to pyemu PE
            ren = pyemu.ParameterEnsemble(pst, resp_par_en)
            ren.to_binary(os.path.join(t_d, "resp_par.jcb"))
            pst.pestpp_options['sweep_parameter_csv_file'] = "resp_par.jcb"
            # need to store real number against spike par
            spike_rel = pd.DataFrame.from_dict(
                dict(
                    resp_par_en.loc[:,
                    load_pars.index].replace(
                            {0.0: np.nan}).stack().index),
                orient='index', columns=['parnme']
            )
            spike_rel['outer_realid'] = realorder
        spike_rel.to_csv(os.path.join(t_d, "resp_reals.csv"), header=False)

        # need to adjust forward run to apply load change
        print("Modifying forward run to add spike array at run time")
        # Modify forward run
        # pull out difference_mtsim_outputs() function from this file
        func_lines = []
        abet_set = set(string.ascii_uppercase)
        abet_set.update(set(string.ascii_lowercase))
        with open('freyberg_sire_invest.py', 'r') as f:
            while True:
                line = f.readline()
                if line.startswith('def apply_loadchange'):
                    func_lines.append(line)
                    while True:
                        line = f.readline()
                        if line == '':
                            func_lines.append('\n')
                            break
                        if line[0] in abet_set:
                            func_lines.append('\n')
                            break
                        func_lines.append(line)
                    break
        with open(os.path.join(t_d, "forward_run.py"), 'r') as frun:
            lines = [line for line in frun.readlines()]
        lineout = []
        for line in lines:
            if line.startswith('def main'):
                lineout.extend(func_lines)
            if 'load2crch' in line and 'def' not in line:
                lineout.append("    apply_loadchange()\n")
            lineout.append(line)
        with open(os.path.join(t_d, "forward_run.py"), 'w') as frun:
            for line in lineout:
                frun.write(line)
        print("Writing .pst")
        pst.pestpp_options['sweep_output_csv_file'] = "responses.csv"  # sweep can only write csv
        pst.pestpp_options['sweep_chunk'] = 500
        pst.control_data.noptmax = -1
        pst.write(os.path.join(t_d, "freyberg.pst"))

        prep_for_condor(m_d, t_d,
                        pestpp_exe="pestpp-swp.exe",
                        bin_dir=os.path.join('..', 'exe'), stomp=True)

    run_and_process(m_d, port=4005, run=run, process=True,
                    pestpp_exe="pestpp-swp.exe", queue=300)
    resp_matrix_from_oe(m_d, process_lo_matrix=True, low_memory=low_memory)
    # pr.print_stats(sort="cumtime")
    # Ensemble UNC
    # invest_unc_scaling(run=run, rd='bmk')


def _build_and_write_pe(chunk, pe, load_arr, load_pars, realorder, spike_rel, out_rel, t_d):
    fmt = f"0{len(str(len(pe.index)))}d"
    for i in chunk:
        ireal_load = load_arr[realorder == str(i)]
        if ireal_load.size > 0:
            print(f'Building par ensemble for sweep '
                  f'{i + 1}/{len(pe.index)}')
            idxs = (realorder == str(i)).nonzero()[0]
            tmp_pe = pe.iloc[[i], :].set_index(
                np.array([i])).rename_axis(
                'real_name').astype(np.float32).reindex(
                idxs, method='pad')
            tmp_pe.loc[:, load_pars.index] = ireal_load
            if i == 0:
                header = True
            else:
                header = False
            # tmp_pe = tmp_pe.set_index(idxs).rename_axis('real_name').astype(np.float32)
            tmp_pe.loc[:, load_pars.index] = tmp_pe.loc[:,
                                             load_pars.index].astype(
                np.int8)
            print(f'Writing par ensemble for sweep '
                  f'{i + 1}/{len(pe.index)}')
            fp = os.path.join(t_d, f'resp_par{i:{fmt}}.csv')
            tmp_pe.to_csv(fp, header=header)
            print(f'Extracting spike/realisation info for'
                  f' par ensemble sweep {i + 1}/{len(pe.index)}')
            stack = tmp_pe.loc[:, load_pars.index].replace(
                {
                    0.0: np.nan}).stack()  # weird pandas 0 and 0.0 issue
            sp_dict = dict(stack.index)
            or_dict = dict(zip(stack.index.droplevel('parnme'),
                               [i] * len(tmp_pe)))
            isect = sp_dict.keys() & spike_rel.keys()
            assert len(isect) == 0, (
                f"realisation numbers already processed, {isect}"
            )
            spike_rel.update(sp_dict)
            out_rel.update(or_dict)


def apply_loadchange(d='.'):
    ch_arr = np.loadtxt(os.path.join(d, 'load.dat'))
    org_arr = np.loadtxt(os.path.join(d, 'org', 'load.dat.orig'))
    load_arr = org_arr + ch_arr
    np.savetxt(os.path.join(d, "load.dat"), load_arr)


def resp_matrix_from_oe(resp_d, process_lo_matrix=False, low_memory=True):
    import multiprocessing as mp
    import mptasks as mpt

    def _process_response_oe(roe, run_id, gwsw=''):
        """
        Set response matrix  index and columns to cell info (e.g. kijt),
            and diff BASE run (no spike)
        :param roe: response matrix ensemble as DF
        :param gwsw: for printing process info
        :return:
        """
        roe.columns = roe.columns.astype(int)
        print(f"Aligning {gwsw} DataFrame columns to runids")
        # incase jcb ordering is messed up somehow
        roe = roe.rename(columns=run_id.input_run_id.to_dict())
        print(f"Extracting {gwsw} response as diff -- subtracting BASE real")
        # want change in concentration
        # if ensemble incl in spiking (BIG BENCHMARK RUN) - need to sub
        # realisation base from each internal real
        # MI roe columns -- getting messy
        nreal = (spike_reals.parnme == spike_reals.iloc[0].parnme).sum()
        outreal = spike_reals.outerreal.to_dict()
        if nreal == 1:
            roe = roe.rename(columns={'BASE': 'BASE0'})
        # if running low order bmk version...
        # need to assign correct outer ensemble realisation ID...
        roe.columns = roe.columns.map(
            lambda x: (int(x) // nreal, outreal[int(x)])
            if not str(x).upper().startswith('BASE')
            else ('BASE', int(str(x).upper().strip("BASE")))
        )
        # same conversion needed for spike reals
        spike_reals.index = spike_reals.index.map(
            lambda x: (x // nreal, outreal[int(x)])).rename(
            ['spikereal', 'real'])
        roe = roe.sub(roe.loc[:, 'BASE'], level=1).drop('BASE', axis=1, level=0)

        print(f"Converting {gwsw} DataFrame axis to kijt")
        roe[['k', 'i', 'j', 't']] = obs.loc[
            roe.index, ['k', 'i', 'j', 't']].astype(int)
        roe['redt'] = roe.t.unique().searchsorted(roe.t)
        roe['date'] = pd.to_datetime(
            mm.modeltime.start_datetime,
            format='%Y-%m-%d %H') + pd.to_timedelta(all_times[roe.t], 'D')
        if ('reachID' in obs.columns
                and not obs.loc[roe.index, 'reachID'].isna().any()):
            roe['reachID'] = obs.loc[roe.index, 'reachID'].astype(int)
            roe = roe.set_index(['reachID', 'k', 'i', 'j', 't', 'redt', 'date'])
        else:
            roe = roe.set_index(['k', 'i', 'j', 't', 'redt', 'date'])
        print("Getting spike cell from resp_reals.csv")
        # get spike cell info from realisation id (format is 'kg_load0kiiijjj')
        # roe.columns = roe.columns.astype(int)
        roe.columns = pd.MultiIndex.from_tuples(
            roe.columns.map(
                lambda x: [x[1], *spike_reals.loc[x, ['i', 'j']].to_list()]
            ).to_list(),
            names=['real', 'i', 'j']
        )
        return roe

    def _process_lo_matrix(roe, low_memory=False, fname=None, npdf='df'):
        """
        Fill missing realisations from sparse run of response matrix ensemble.

        :param roe: Sparse response observation ensemble.
                     Preprocessed to location and time index for cell response
                     and spike cell ij and realisation columns.
                     Both index and columns are expected to be pd.MultiIndex()
        :param low_memory: Run processing trying to keep memory usage low
                            -- recommended for sparse ensemble response matrix
        :param fname: Path to safe output file.
                         Filename will be appended with realisation number when
                         saving response matrices.
        :param npdf: Flag to specify whether multiprocess infilling is done
                      using sliced dataframes ("df") or if reference (roe) is
                      accessed from shared memory as numpy ("np") array.
        :return:
        """
        # roe = roe.drop(0, axis=1, level='real')  # TODO only for 0 ensemble.
        print("Set near zero to zero")
        # oe[oe < 1e-12] = 0
        roe = roe.where(roe >= 1.e-12, 0.)
        print("Setting up mp methods")
        if npdf == 'np':
            chunk_len = 5
        else:
            chunk_len = 5  # smaller number == more processors
        # ij = roe.columns.droplevel('real').unique()
        if not low_memory:  # need MASSIVE df for filling
            fulla = roe.stack('real').unstack('real')  # reorders levels (i, j, real)
        nreal = len(roe.columns.unique('real'))
        fmt = f"0{len(str(nreal))}d"
        num_chunk_floor = nreal // chunk_len
        main_chunks = np.arange(nreal)[
                      : num_chunk_floor * chunk_len].reshape(
            [-1, chunk_len]).tolist()
        remainder = np.arange(nreal)[
                    num_chunk_floor * chunk_len:].tolist()
        chunks = main_chunks + [remainder]
        if not low_memory:  # need MASSIVE df for filling
            fulla = roe.stack('real').unstack('real')  # reorders levels (i, j, real)
            manager = mp.Manager()
            fulld = manager.dict()
        else:
            fulld = None
        if npdf == 'np':
            print("Prepping for numpy fill using share")
            idxs = roe.index
            oecols = roe.columns
            oe = roe.values
            print("creating shared block")
            shr, oe = _create_shared_block(oe)
            print("Distributing processing...")
            with mp.get_context("spawn").Pool() as pool:
                for c in chunks:
                    ou = [
                        pool.apply_async(
                            mpt._fill_realisation_response_fromnp,
                            args=(shr.name, oe.shape, c, idxs, oecols, fname,
                                  fmt, fulld)
                        )
                    ]
                    if len(idxs) > 10000:
                        time.sleep(20)
                [r.get() for r in ou]
                print("closing pool")
                pool.close()

                print("joining pool")
                pool.join()

            shr.close()
            shr.unlink()
        else:
            print("Prepping for df fill (passing mean and part)")
            print("Calc mean")
            meanresp = roe.mean(axis=1, level=['i', 'j'])
            print("Distributing processing...")
            ou = []
            with mp.get_context("spawn").Pool() as pool:
                for c in chunks:
                    partoe = roe.loc[:, (slice(None), slice(None), c)]
                    ou.append(
                        pool.apply_async(
                            mpt._fill_realisation_response_fromdf,
                            args=(c, partoe, meanresp, fname, fmt, fulld)
                        )
                    )
                    if len(partoe) > 10000:
                        time.sleep(20)
                partsumdfs = [r.get() for r in ou]
                print("closing pool")
                pool.close()

                print("joining pool")
                pool.join()
            print("Writing mean response matrix...")
            fullmean = sum(partsumdfs) / nreal
            fullmean = fullmean.reorder_levels(['i', 'j'], axis=1)
            fullmean = fullmean.sort_index(axis=1, level=['i', 'j'])
            fullmean.astype('float32').to_hdf(
                os.path.join(resp_d, 'mean_fresp_mat_gw.hdf'), 'df', mode='w',
                complib='blosc:lz4hc', complevel=9
            )
        if low_memory:
            # Calc standard deviation -- need to load in all reals again
            print("Calculating elementwise response matrix std...")
            fullstd = get_respmat_std(fname, fullmean.divide(10.))
            print("Writing std response matrix...")
            fullstd.astype('float32').to_hdf(
                os.path.join(resp_d, 'std_fresp_mat_gw.hdf'), 'df', mode='w',
                complib='blosc:lz4hc', complevel=9
            )
        if not low_memory:
            if fulld:
                print("Building infill response df...")
                udf = pd.DataFrame.from_dict(dict(fulld))
                udf.columns = udf.columns.rename(['i', 'j', 'real'])
                print("Filling full response df...")
                fulla.update(udf, overwrite=False)
            if fname:
                print(f"    Writing response matrix hdf: {fname}")
                fulla.astype('float32').to_hdf(fname, 'df', mode='w',
                                               complib='blosc:lz4hc',
                                               complevel=9)
            return fulla

    print("Processing response matrix sweep results")
    print("    Reading pst")
    pst = pyemu.Pst(os.path.join(resp_d, "freyberg.pst"))
    obs = pst.observation_data
    print("    Read Sims")
    mm = flopy.modflow.Modflow.load("freyberg.nam", model_ws=resp_d,
                                    check=False, verbose=False,
                                    load_only=['BAS6', 'DIS', 'SFR'])
    mt = flopy.mt3d.Mt3dms.load("freyberg_mt.nam", model_ws=resp_d,
                                modflowmodel=mm)
    all_times = np.cumsum(mt.btn.perlen.array)
    print("    Getting all obsnames and kijts")
    reachdata = pd.DataFrame(mm.sfr.reach_data).set_index('reachID')
    stdt = pd.to_datetime(mm.modeltime.start_datetime, format='%Y-%m-%d %H')
    sire._parse_kijt_resp_obs(obs, stdt, all_times, reachdata)

    runid = pd.read_csv(os.path.join(resp_d, 'runid.csv'), index_col=0)
    if resp_d == 'test':
        runid = runid.append(pd.DataFrame.from_dict(
            {i: "BASE" + str(i) for i in range(69500, 69600)}, orient='index',
            columns=runid.columns))
    spike_reals = pd.read_csv(os.path.join(resp_d, 'resp_reals.csv'),
                              index_col=0,
                              names=['real', 'parnme', 'outerreal'])
    if spike_reals.outerreal.isna().all():
        nreal = (spike_reals.parnme == spike_reals.iloc[0].parnme).sum()
        spike_reals.loc[:, ['outerreal']] = np.mod(spike_reals.index, nreal)
    spike_reals[['i', 'j']] = spike_reals.parnme.apply(
        lambda x: pd.Series([int(dict([ent.split(':')
                                       for ent in x.split('_')
                                       if ':' in ent]).get(i))
                             for i in ['i', 'j']])
    )

    print("Reading GW concentration output jcbs")
    gwoe_fnames = [fname for fname in os.listdir(resp_d)
                   if all([s in fname for s in ['ucn', 'jcb']])]
    gwoe = []
    for i, oe_fname in enumerate(gwoe_fnames):
        print(f"    Reading jcb {i + 1}/{len(gwoe_fnames)}")
        gwoe.append(pyemu.ObservationEnsemble.from_binary(
            pst, os.path.join(resp_d, oe_fname))._df.astype('float32').T)
    print("Concatenating GW response DataFrames")
    gwoe = pd.concat(gwoe)
    if resp_d == 'test':
        gwoe = pd.concat([gwoe,
                          pd.DataFrame(0., index=gwoe.index,
                                       columns=range(69500, 69600))], axis=1)
    print("Processing GW response DataFrames")
    gwoe = _process_response_oe(gwoe, runid, gwsw='GW')
    resp_hdf_fname = os.path.join(resp_d, 'resp_mat_gw.hdf')
    print(f"    Writing response matrix hdf: {resp_hdf_fname}")
    # preserve column order
    gwoe = gwoe.reorder_levels(['i', 'j', 'real'], axis=1)
    gwoe = gwoe.sort_index(axis=1, level=['i', 'j'])
    gwoe.astype('float32').to_hdf(resp_hdf_fname, 'df', mode='w',
                                  complib='blosc:lz4hc', complevel=9)
    if process_lo_matrix:
        # output index -- means we can process response matrices as pure numpy == faster!
        gwoe.index.to_frame().reset_index(drop=True).to_csv(
            os.path.join(resp_d, 'oeindex.csv')
        )
        gwoe.columns.to_frame().reset_index(drop=True).to_csv(
            os.path.join(resp_d, 'colorder.csv'))

        resp_hdf_fname = os.path.join(resp_d, 'fresp_mat_gw.hdf')
        resp_blc_fname = resp_hdf_fname.replace('.hdf', '.blc')
        gwoe = _process_lo_matrix(gwoe,
                                  fname=resp_blc_fname, low_memory=low_memory)


def get_respmat_std(fname, respmean):
    from pathlib import Path
    import pickle
    import blosc
    import tqdm
    fname = Path(fname)
    d = fname.parent
    mat = None
    sstring = f"{fname.stem}_*{fname.suffix}"
    respfiles = [ff for ff in d.glob(sstring)]
    try:
        sum_x_sub_xbar_sq = np.zeros(respmean.shape)
    except AttributeError:
        # respmean could be a filename
        respmean = pd.read_hdf(respmean)
        sum_x_sub_xbar_sq = np.zeros(respmean.shape)
    sumx = np.zeros(respmean.shape)
    for f in tqdm.tqdm(respfiles):
        with open(f, 'rb') as fp:
            shape, dtype = pickle.load(fp)
            c = fp.read()
        if mat is None:  # array should be around the same size can reuse allocation
            mat = np.empty(shape, dtype)
        blosc.decompress_ptr(c, mat.__array_interface__['data'][0])
        sum_x_sub_xbar_sq = sum_x_sub_xbar_sq + (np.divide(mat, 10.) - respmean.values)**2
        sumx = sumx + np.divide(mat, 10.)
    std = pd.DataFrame((sum_x_sub_xbar_sq/(len(respfiles)-1))**0.5,
                       index=respmean.index,
                       columns=respmean.columns)
    chkmean = pd.DataFrame(sumx/len(respfiles), index=respmean.index,
                           columns=respmean.columns)
    assert np.isclose(chkmean, respmean).all(), (chkmean.head(),
                                                 respmean.head())
    return std


def test_transient_sire(pcts=None, testload=None):
    """
    Testing the transient sire process
    Reads in large response matrix and ensemble output hdf files written by prep_for_sire()
    Defines a random loading scenario
    :param pcts:
    :return:
    """
    import matplotlib.animation as animation
    import matplotlib.cm as cm
    import matplotlib.colors as colors
    import matplotlib.pyplot as plt
    import matplotlib.patches as patches
    import matplotlib.path as path
    from itertools import cycle

    def init_ani():
        # initialise animation (not sure if this is req.)
        im.set_data(np.zeros([mt.nrow, mt.ncol]))
        # patch = None
        # do need to return both objects that are to be updated
        return [im, patch, sim]

    def update_ani(frame, ars, sars, df, k, ts, cvmax, svmax, yrs):
        """
        Animation update function
        :param frame: automatic frame counter
        :param ars: 3D array for plotting (t,i,j)
        :param k: current layer (is this needed?)
        :param ts: all output times in days
        :param cvmax: max concentration TODO may not be used
        :param svmax: max SW concentration TODO
        :param yrs: Timestamps for output times
        :param df: Response uncertainty ensemble dataframe
        :return:
        """
        t = ts[frame]  # time of timeslice
        p = ars[frame]  # timeslice array to plot
        year = yrs[frame]  # year of timeslice
        resp = gwresponse.loc[(k, slice(None), slice(None),
                              slice(None), frame,
                              slice(None)), :]  # timeslice
        print(f"updating CDF plots for layer {k} at time {t}")
        for i, (_, sl) in enumerate(resp.iterrows()):  # loop over timeslice
            v = np.zeros(len(sl))
            if not sl.isna().all():
                r = (sl.min(), sl.max())
                v, b = np.histogram(sl, nbins, range=r)
                lf = np.array(b[:-1])
                r = np.array(b[1:])
                vverts[i][0, 0] = lf[0]  # update leftmost bin
                vverts[i][1::2, 0] = lf  # update all bins
                vverts[i][2::2, 0] = r
            tp = np.cumsum(v)  #  get new counts
            vverts[i][1::2, 1] = tp  # update CDF (left and right verts)
            vverts[i][2::2, 1] = tp

        print(f'adding concentration for layer {k} at time {t}')
        im.set_data(p)  # set imshow data
        if sars is not None:  # TODO
            sfc_ar = sars[frame]
            sim.set_data(sfc_ar)
        mpax.set_title(f'{pct*100:.0f}% $\Delta$conc < displayed\n'
                       f'Layer {k + 1} after {t / 365.:.1f} yrs @ {year}')
        return [im, patch, sim]

    # Setup color cycle for CDF patches
    name = "Set3"
    cmap = cm.get_cmap(name)
    cs = cmap.colors
    cyc = cycle(cs)

    # include SW response?
    sfc = False  # for surface water inclusion

    # Set up animation stuff
    writer = animation.FFMpegWriter(fps=2, metadata=dict(artist='BH'),
                                    bitrate=1800)
    resp_d = "master_resp"  # where e.g. model is
    sire_d = "transient_sire"  # where simulation outputs are
    savespace = os.path.join(resp_d, 'plots')
    if not os.path.exists(savespace):
        os.mkdir(savespace)
    print("Read Sims")
    mm = flopy.modflow.Modflow.load("hauraki_transient.nam", model_ws=resp_d,
                                    check=False, verbose=False,
                                    load_only=['BAS6', 'DIS', 'SFR'])
    mt = flopy.mt3d.Mt3dms.load("hauraki_transient.mt3d.nam", model_ws=resp_d,
                                modflowmodel=mm, load_only=['BTN'])
    reachdata = pd.DataFrame(mm.sfr.reach_data).set_index('reachID')
    print("GW")
    print("    Reading response matrix")
    gw_respmat = pd.read_hdf(os.path.join(sire_d, 'resp_mat_gw.hdf'))
    print("    Reading uncertainty ensembles")
    gwuncoe = pd.read_hdf(os.path.join(sire_d, 'demean_unc_gw.hdf'))
    if sfc:
        print("SW")
        print("    Reading response matrix")
        # SW might be duplicated in cells
        sw_respmat = pd.read_hdf(os.path.join(sire_d, 'resp_mat_sw.hdf'))
        print("    Reading uncertainty ensembles")
        swuncoe = pd.read_hdf(os.path.join(sire_d, 'demean_unc_sw.hdf'))
    # Colecting some time info stuff
    kper = gw_respmat.index.get_level_values('t').unique()
    all_times = np.cumsum(mt.btn.perlen.array)
    dates = gw_respmat.index.get_level_values('date').unique() # .strftime('%Y-%m-%d')

    # Define test scenario as i,j loadchange series
    if testload is None:
        test_scenario = pd.Series(np.random.random(len(gw_respmat.columns)),
                                  index=gw_respmat.columns) * 10
    else:
        test_scenario = pd.Series([testload[i, j]
                                   for i, j in gw_respmat.columns],
                                  index=gw_respmat.columns)

    # Apply scenario - dot product of response matrix and loadchange series
    # Cast response to uncertainty matrix
    # Uncertain scenario response
    gwresponse = gwuncoe.mul(gw_respmat.dot(test_scenario), axis=0)
    if sfc:
        swresponse = swuncoe.mul(sw_respmat.dot(test_scenario), axis=0)

    # Testing zone - just looking at lay 1
    k = 0
    # Get min and max for layer (for cdf plots)
    mn = gwresponse.loc[
         (k, slice(None), slice(None),
          slice(None), slice(None), slice(None)), :].min().min()
    mx = gwresponse.loc[
         (k, slice(None), slice(None),
          slice(None), slice(None), slice(None)), :].max().max()
    # For CDF hist plots
    # initial hist
    # nn = []
    # bbins = []
    nbins = 100
    nverts = 1 + (nbins * 2)  # 2 vertices per bin + first point
    codes = np.ones(nverts, int) * path.Path.LINETO  # defines the type of vert
    codes[0] = path.Path.MOVETO  # first need to be MOVETO
    vverts = []  # container for all vert sets (one set per cell)
    # Loop over shifted response uncertainty matrix
    for _, s in gwresponse.loc[
                (0, slice(None), slice(None),
                 slice(None), 0, slice(None)), :].iterrows():
        verts = np.zeros((nverts, 2))  # incase no CDF for cell (all nans)
        # n = np.zeros(nbins)
        # bins = np.linspace(0, mx, nbins+1)  # default bin positions
        if not s.isna().all():  # only if there is a CDF to construct
            range = (s.min(), s.max())  # np histogram so can pass range
            n, bins = np.histogram(s, nbins, range=range)
            # get the corners of the bins for the histogram
            left = np.array(bins[:-1])  #
            right = np.array(bins[1:])
            # bottom = np.zeros(len(left))
            top = np.cumsum(n)

            verts[0, 0] = left[0]  # first point at x axis and far left
            verts[0, 1] = 0.
            verts[1::2, 0] = left  # xcoord of lefthand edges
            verts[1::2, 1] = top  # ycoord lefthand edges
            verts[2::2, 0] = right  # xcoord righthand edges
            verts[2::2, 1] = top  # ycoord of righthand edges
        # nn.append(n)
        # bbins.append(np.cumsum(bins))
        vverts.append(verts)  # all zero if no CDF

    # set up container for e.g. array
    shp4d = [len(kper), mm.nlay, mm.nrow, mm.ncol]
    shp3d = [len(kper), mm.nrow, mm.ncol]
    mask = np.broadcast_to(mt.btn.icbund.array == 0, shp4d).copy()
    smask = np.ones([mm.nrow, mm.ncol])
    smask[tuple(reachdata[['i', 'j']].drop_duplicates().values.T)] = 0.0
    smask = np.broadcast_to(smask, shp3d).copy()
    scen_ar = np.zeros(shp4d[2:])  # define array for imshow
    scen_ar = np.ma.masked_array(scen_ar, mask[0, 0])
    scen_ar[tuple(np.array(
        test_scenario.index.to_list()).T)] = test_scenario.values
    if pcts is not None:
        print("Generating timeseries movie files")
        # Test timeseries plots through de-meaned distibutions
        for pct in pcts:  # Loop over all desired risk values
            patch = None  # reset CDF patches
            print(f"    Movie for quantile {pct}")
            # extract percentile
            print(f"        Extracting quantile {pct}")
            gw_pctile = gwresponse.quantile(pct, axis=1, interpolation='linear')
            # GW conc array for imshow
            ar = np.zeros(shp4d)  # define array for imshow
            ar = np.ma.masked_array(ar, mask)
            ar[tuple(gw_pctile.reset_index()[
                         ['redt','k', 'i', 'j']].values.T)] = gw_pctile.values
            plar = ar[:, k, :, :]  # plotting array
            minc = np.nanmin(plar)
            maxc = np.nanmax(plar)
            smp = cm.ScalarMappable(
                norm=colors.Normalize(minc, maxc),
                cmap=cm.plasma)
            smp.set_array([])

            # Initialise figure for plot
            if sfc and k == 0:
                grow = 6
                hr = [0.6, 0.6, 1, 0.1, 0.1, 0.1]
            else:
                grow = 4
                hr = [0.6, 0.6, 1, 0.2]
            fig = plt.figure(figsize=(12, 12))
            pgrid = plt.GridSpec(grow, 2, hspace=0.1,
                                 height_ratios=hr)
            mpax = plt.subplot(pgrid[:3, 0])
            cbax = plt.subplot(pgrid[3, 0])
            if sfc and k == 0:
                scbax = plt.subplot(pgrid[5, 0])
            scax = plt.subplot(pgrid[:2, 1])
            cdfax = plt.subplot(pgrid[2:, 1])

            # Add scenario plot
            scim = scax.imshow(scen_ar)
            cscb = plt.colorbar(scim, ax=scax)
            cscb.set_label('$\Delta$Load (kgd$^{-1}$)')

            # Build and add first CDF patches at t=0
            for verts in vverts:
                barpath = path.Path(verts, codes)
                patch = patches.PathPatch(
                    barpath, facecolor=None, fill=None, edgecolor=next(cyc),
                    lw=1.5)
                cdfax.add_patch(patch)
            cdfax.set_xlim(mn, mx)
            cdfax.set_ylim(0, gwresponse.shape[1]+1)
            cdfax.set_xlabel('Cell $\Delta$conc uncertainty CDF (kgm$^{-3}$)')

            # Add colorbar for response (smp) defined for array above
            cbar = plt.colorbar(smp, cax=cbax, orientation='horizontal')
            cbar.set_label('Response $\Delta$conc (kgm$^{-3}$)')
            # Add blank array
            im = mpax.imshow(plar[0],
                             animated=True, vmin=minc, vmax=maxc, cmap='plasma')

            # Possibly add SW conc
            if sfc and k == 0:
                sar = np.zeros(shp3d)
                sar = np.ma.masked_array(sar, smask)
                sw_pctile = swresponse.quantile(pct, axis=1,
                                                interpolation='linear')
                sar[tuple(sw_pctile.reset_index().drop_duplicates(
                    ['redt', 'i', 'j'])[['redt', 'i', 'j']].values.T)] = \
                    sw_pctile.reset_index().drop_duplicates(
                        ['redt', 'i', 'j'])[pct].values  # TODO
                sminc = np.nanmin(sar)
                smaxc = np.nanmax(sar)
                ssmp = cm.ScalarMappable(
                    norm=colors.Normalize(sminc, smaxc),
                    cmap=cm.cool)
                ssmp.set_array([])
                # Add colorbar for sw response (smp) defined for array above
                scbar = plt.colorbar(ssmp, cax=scbax, orientation='horizontal')
                scbax.xaxis.tick_top()

                sim = mpax.imshow(sar[0],
                                  animated=True, vmin=sminc, vmax=smaxc,
                                  cmap='cool')
            else:
                sar = None
            # Build animation passing fig, function and number of frames
            # fargs passed to update function:
            #     full array to plot (tslice accessed in function),
            #     layer,
            #     times (in days) for output times,
            #     concentration max, # TODO: may not be used
            #     SW concn max, # TODO
            #     output dates for all output times,
            #     Dataframe unc response
            ani = animation.FuncAnimation(
                fig, update_ani, len(kper),
                fargs=[plar, sar, gwresponse, k, all_times[kper], maxc, 1,
                       dates], init_func=init_ani, blit=True, repeat=False)
            ani.save(
                os.path.join(savespace,
                             f'{mt.name}_egresp_'
                             f'{k + 1}_{str(pct)}.mp4'),
                writer=writer)


def fosm_stds(runjco=True):
    t_d = 'template_fosm'
    m_d = 'master_fosm'
    r_d = '.'
    it_d = os.path.join(r_d, 'new_temp')
    pstfile = 'freyberg.pst'
    if runjco:
        if os.path.exists(t_d):
            shutil.rmtree(t_d)
        print(f"Copying PEST template dir to {t_d}")
        shutil.copytree(it_d, t_d)
        pst = pyemu.Pst(os.path.join(t_d, pstfile))
        # fix errant parubnd
        pars = pst.parameter_data
        dnpars = pars.index.str.startswith('multiplier_rc11')
        loadpars = pars.index.str.startswith('multiplier_load')
        pars.loc[loadpars | dnpars, 'parubnd'] = 10
        runidpar = pars.index.str.startswith('runid')
        pars.loc[runidpar, 'parlbnd'] = 0
        pars.loc[runidpar, 'parlbnd'] = 100
        pars.loc[runidpar, 'partrans'] = 'fixed'
        pst.parameter_groups.derinc = 0.005
        pst.parameter_groups.forcen = 'always_3'
        pst.parameter_groups.derincmul = 1.0
        pst.control_data.noptmax = -1
        pst.write(os.path.join(t_d, "freyberg.pst"))
        prep_for_condor(m_d, t_d,
                        pestpp_exe="pestpp-glm.exe",
                        bin_dir=os.path.join('..', 'exe'), stomp=True)
        run_and_process(m_d, port=4004, run=runjco, process=False,
                        pestpp_exe="pestpp-glm.exe", queue=300)
    pst = pyemu.Pst(os.path.join(m_d, pstfile))
    pars = pst.parameter_data
    id_names = pars.index.str.startswith('runid')
    load_names = pars.index.str.contains("load")  # drop load pars?!?
    par_names = list(pars.loc[(~id_names) & (~load_names), 'parnme'])

    pst.parameter_data = pst.parameter_data.loc[par_names, :]
    cov = pyemu.Cov.from_parameter_data(pst, sigma_range=6.0)
    jco = pyemu.Jco.from_binary(
        os.path.join(m_d, pstfile.replace('pst', 'jcb')))
    cov.to_ascii(os.path.join(m_d, "parcov_diag.dat"))
    cov.to_binary(os.path.join(m_d, "parcov_diag.jco"))
    fore_jco = jco.get(col_names=par_names)
    obs = pst.observation_data
    obs.loc[:, "weight"] = 0.0

    la = pyemu.LinearAnalysis(pst=pst, parcov=cov, forecasts=fore_jco.T,
                              verbose=True)
    prior_fore = la.prior_forecast
    with open(os.path.join(m_d, "forecast_std.csv"), 'w') as f:
        f.write("obsnme,obsval\n")
        for n, v in prior_fore.items():
            f.write("{0},{1}\n".format(n, np.sqrt(v)))
    res = pst.res
    var = pd.DataFrame.from_dict(prior_fore, orient='index',
                                 columns=['variance'])
    normvar = var.div(res.modelled ** 2, axis=0)
    normvar.to_csv(os.path.join(m_d, "forecast_normvar.csv"),
                   header=True, index=True)


def test_apply_load(d='master_lobmk2_resp', up2k=None):
    import pathlib
    import mptasks as mpt

    def calc_response_ensemble(files, test_scenario, stop=None):
        nproc = 9
        step = int(np.ceil(len(files) / nproc))
        chunks = [files[i:i + step] for i in range(0, len(files), step)]
        manager = mp.Manager()
        resp = manager.list()
        # resp = manager.dict()
        with mp.get_context("spawn").Pool(processes=nproc) as pool:
            for c in chunks:
                print(f"Building response for reals {c}")
                ou = [
                    pool.apply_async(
                        mpt._read_and_apply_sire,
                        args=(c, stop, test_scenario, resp)
                    )
                ]
            [r.get() for r in ou]
            print("closing pool")
            pool.close()

            print("joining pool")
            pool.join()
        resp = pd.concat(list(resp), axis=1).astype(np.float32)
        # resp = pd.DataFrame(dict(resp)).sort_index(axis=1).astype(np.float32)
        return resp

    shp = (124, 70)
    # loading = np.zeros(shp)
    # loading[0:10, 0:10] = 23.7
    # loading[0:10, 10:] = -43.7
    # generate dummy loading
    loading = 2 * (2 * np.random.random(shp) - 1)
    # loading[:] = 100
    loading[7:11, 7:9] += 10
    loading[7:11, 11:13] += -10

    s = f"{'?':?<2}"
    files = sorted(
        [x.name for x in pathlib.Path(d).glob(f"fresp_mat_gw_0_{s}.hdf")]
            )
    files = [pathlib.Path(d).joinpath(f) for f in files]
    hdff = os.path.join(d, 'fresp_mat_gw_9_blosc_lz4hc.tbl')
    index = pd.read_csv(os.path.join(d, 'goeindex_0.csv'), index_col=0)
    if up2k is not None:
        idx = np.argmax(index.k.values >= up2k)
        if idx == 0:
            idx = None
    else:
        idx=None
    eg = pd.read_csv(os.path.join(d, 'colorder.csv'), index_col=0)
    test_scenario = np.array(
        [loading[i, j] for i, j in eg.set_index(['i','j']).index.unique()],
        dtype=np.float32
    )
    # for f in files:
    # pr = cProfile.Profile()
    # pr.enable()
    t0 = time.perf_counter()
    np_respmat2 = calc_response_ensemble(files, test_scenario, stop=idx)
    np_respmat2.index = pd.MultiIndex.from_frame(index)
    t1 = time.perf_counter()
    print(f"response ensemble gen pure numpy from files took {t1 - t0}")
    # pr.disable()
    # pr.print_stats(sort="cumtime")

    test=None


def test_readwrite(d='master_lobmk2_resp', complib=None,  complevel=0):
    import time
    import cProfile
    import pathlib
    import tables
    shp = (40, 20)
    # loading = np.zeros(shp)
    # loading[0:10, 0:10] = 23.7
    # loading[0:10, 10:] = -43.7
    loading = 2 * (2 * np.random.random(shp) - 1)
    # loading[:] = 100
    loading[7:11, 7:9] += 10
    loading[7:11, 11:13] += -10
    s = f"{'?':?<2}"
    files = sorted(
        [x.name for x in pathlib.Path(d).glob(f"fresp_mat_gw{s}.hdf")]
            )
    eg = pd.read_hdf(pathlib.Path(d).joinpath(files[0]), stop=None)
    test_scenario = pd.Series(
        [loading[i, j] for i, j in eg.columns.droplevel('real')],
        index=eg.columns.droplevel('real')
    )

    # for f in files read all and write 1:
    def write_hdf(files, complevel=0, complib=None):
        if complevel > 0:
            if complib is None:
                    complib = 'zlib'
            if ":" in complib:
                flib = complib.replace(":", "_")
            else:
                flib = complib
        else:
            flib = complib

        fname = pathlib.Path(d).joinpath(f"fresp_mat_gw_{complevel}_{flib}.hdf")
        with pd.HDFStore(fname, mode='w',
                         complevel=complevel, complib=complib) as st:
            for i, f in enumerate(files):
                df = pd.read_hdf(pathlib.Path(d).joinpath(f))
                st.put(f'r_{i}', df)
        return fname

    def calc_response_ensemble(fname, keys, test_scenario):
        gwresp = {
            int(''.join([s for s in ke if s.isdigit()])):
                pd.read_hdf(fname, ke).droplevel('real',
                                                axis=1).dot(test_scenario)
                for ke in keys
                 }
        gwresp = pd.DataFrame.from_dict(gwresp, orient='columns').sort_index(axis=1)
        return gwresp

    def calc_response_ensemble_from_store(fname, test_scenario, stop=None):
        with pd.HDFStore(fname, 'r') as st:
            gwresp = pd.concat(
                [
                    st.select(ke, stop=stop).droplevel('real', axis=1).dot(test_scenario)
                    for ke in st.keys()
                ], axis=1
            )
        return gwresp

    def calc_response_ensemble_from_multi(files, test_scenario, stop=None):
        gwresp = pd.concat(
            [
                pd.read_hdf(pathlib.Path(d).joinpath(fname), stop=stop).droplevel('real', axis=1).dot(test_scenario)
                for fname in files
            ], axis=1
        )
        return gwresp

    def calc_response_ensemble_from_numpy(fname, test_scenario):
        with tables.open_file(fname, 'r') as h5_file:
            keys = [node.name for node in h5_file.iter_nodes('/')]
            gwresp = {
                int(''.join([s for s in ke if s.isdigit()])):
                    h5_file.root[ke].read().dot(test_scenario) for ke in keys
            }
        gwresp = pd.DataFrame.from_dict(gwresp, orient='columns').sort_index(axis=1)
        return gwresp

    def calc_response_ensemble_from_numpy_from_files(files, test_scenario):
        gwresp = {}
        for f in files:
            fname = pathlib.Path(d).joinpath(f)
            r = int(''.join([s for s in f if s.isdigit()]))
            with tables.open_file(fname, 'r') as h5_file:
                gwresp[r] = h5_file.root[f'r_{r}'].read().dot(test_scenario)
        gwresp = pd.DataFrame.from_dict(gwresp, orient='columns').sort_index(axis=1)
        return gwresp

    # level = 0
    # lib = None
    fname = write_hdf(files, complevel=complevel, complib=complib)
    with pd.HDFStore(fname) as hdf:
        k = hdf.keys()
    t0 = time.perf_counter()
    resp = calc_response_ensemble(fname, k, test_scenario)
    t1 = time.perf_counter()
    print(f"{complevel, complib} took {t1-t0}")
    #
    # t0 = time.perf_counter()
    # resp = calc_response_ensemble_from_store(fname, test_scenario)
    # t1 = time.perf_counter()
    # print(f"{level, lib} from store took {t1-t0}")
    # #
    # # for lib in ['zlib', 'lzo', 'bzip2',
    # #             'blosc:blosclz', 'blosc:lz4', 'blosc:lz4hc',
    # #             'blosc:snappy', 'blosc:zlib', 'blosc:zstd']:
    # #     level = 1
    # #     fname = write_hdf(files, complevel=level, complib=lib)
    # #     with pd.HDFStore(fname) as hdf:
    # #         k = hdf.keys()
    # #     t0 = time.perf_counter()
    # #     resp = calc_response_ensemble(fname, k, test_scenario)
    # #     t1 = time.perf_counter()
    # #     print(f"{level, lib} took {t1-t0}")
    #
    # for lib in ['blosc:lz4', 'blosc:lz4hc']:
    #     for level in [9]:
    #         fname = write_hdf(files, complevel=level, complib=lib)
    #         with pd.HDFStore(fname) as hdf:
    #             k = hdf.keys()
    #         t0 = time.perf_counter()
    #         resp = calc_response_ensemble(fname, k, test_scenario)
    #         t1 = time.perf_counter()
    #         print(f"{level, lib} took {t1 - t0}")
    #         t0 = time.perf_counter()
    #         resp = calc_response_ensemble_from_store(fname, test_scenario)
    #         t1 = time.perf_counter()
    #         print(f"{level, lib} from store took {t1 - t0}")

    # t0 = time.perf_counter()
    # resp = calc_response_ensemble_from_multi(files, test_scenario)
    # t1 = time.perf_counter()
    # print(f"read from multi took {t1 - t0}")

    # level = 0
    # lib = None
    fname = write_table(files, complevel=complevel, complib=complib)
    t0 = time.perf_counter()
    np_respmat = calc_response_ensemble_from_numpy(fname, test_scenario)
    t1 = time.perf_counter()
    print(f"{complevel, complib} with pure numpy took {t1-t0}")


    files = sorted(
        [x.name for x in pathlib.Path(d).glob(f"fresp_mat_gwa{s}.hdf")]
            )
    t0 = time.perf_counter()
    np_respmat2 = calc_response_ensemble_from_numpy_from_files(files, test_scenario)
    t1 = time.perf_counter()
    print(f"{complevel, complib} with pure numpy from files took {t1 - t0}")
    test=None
    #### 'blosc:lz4hc', 'blosc:snappy', 'blosc: lz4 ' seem to be fastest for toy model
    # level doesn't make much dif to read speed (write might be slower but meh!)


def write_table(d, files, complevel=0, complib=None):
    if complevel > 0:
        if complib is None:
                complib = 'zlib'
        if ":" in complib:
            flib = complib.replace(":", "_")
        else:
            flib = complib
    else:
        flib = complib
    fname = os.path.join(d, f"fresp_mat_gw_{complevel}_{flib}.tbl")
    with tables.open_file(fname, mode="w",
                          filters=tables.Filters(complevel=complevel,
                                                 complib=complib)) as st:
        for i, f in enumerate(files):
            with tables.open_file(os.path.join(d,f), mode='r') as infile:
                dat = infile.root[f'r_{i}'].read()
            st.create_carray('/', f'r_{i}', obj=dat)
    return fname


def _create_shared_block(ar):
    # a = np.ones(shape=(5000, 5000), dtype=np.float32)  # Start with an existing NumPy array
    shm = mp.shared_memory.SharedMemory(create=True, size=ar.nbytes)
    # # Now create a NumPy array backed by shared memory
    np_array = np.ndarray(ar.shape, dtype=np.float32, buffer=shm.buf)
    np_array[:] = ar[:]  # Copy the original data into shared memory
    return shm, np_array


def mp_share_test(npdf='np'):
    import time
    """np slow""" # todo
    fulld=None
    print("Reading initial hdf")
    # hdfname = os.path.join('..', 'master_resp', 'resp_mat_gw_0.hdf')
    hdfname = os.path.join('master_lobmk2_resp', 'resp_mat_gw.hdf')
    oe = pd.read_hdf(hdfname)
    print("Set near zero to zero")
    # oe[oe < 1e-12] = 0
    oe = oe.where(oe >= 1.e-12, 0.)
    print("Setting up mp methods")
    chunk_len = 5
    # chunk_len = 10
    nreal = len(oe.columns.unique('real'))
    fmt = f"0{len(str(nreal))}d"
    num_chunk_floor = nreal // chunk_len
    main_chunks = np.arange(nreal)[
                  : num_chunk_floor * chunk_len].reshape(
        [-1, chunk_len]).tolist()
    remainder = np.arange(nreal)[
                num_chunk_floor * chunk_len:].tolist()
    chunks = main_chunks + [remainder]
    # manager = mp.Manager()
    hdfname = hdfname.replace('resp_', 'fresp_')
    if npdf == 'np':
        print("Prepping for numpy fill using share")
        idxs = oe.index
        oecols = oe.columns
        oe = oe.values
        print("creating shared block")
        shr, oe = _create_shared_block(oe)
        print("Distributing processing...")
        with mp.get_context("spawn").Pool() as pool:
            for c in chunks:
                ou = [
                    pool.apply_async(
                        mpt._fill_realisation_response_fromnp,
                        args=(shr.name, oe.shape, c, idxs, oecols, hdfname,
                              fmt, fulld)
                    )
                ]
            [r.get() for r in ou]
            print("closing pool")
            pool.close()

            print("joining pool")
            pool.join()

        shr.close()
        shr.unlink()
    else:
        print("Prepping for df fill (passing mean and part)")
        print("Calc mean")
        meanresp = oe.mean(axis=1, level=['i', 'j'])
        print("Distributing processing...")
        with mp.get_context("spawn").Pool() as pool:
            for c in chunks:
                partoe = oe.loc[:, (slice(None), slice(None), c)]
                ou = [
                    pool.apply_async(
                        mpt._fill_realisation_response_fromdf,
                        args=(c, partoe, meanresp, hdfname, fmt, fulld)
                    )
                ]
                if len(partoe) > 10000:
                    time.sleep(20)
            [r.get() for r in ou]
            print("closing pool")
            pool.close()

            print("joining pool")
            pool.join()

    # processes = []
    # for i in range(mp.cpu_count()):
    #     _process = mp.Process(target=_proc, args=(shr.name, np_array.shape, i))
    #     processes.append(_process)
    #     _process.start()
    #
    # for _process in processes:
    #     _process.join()

    # print("Final array")
    # print(np_array[:10])
    # print(np_array[10:])


def response_read_test(d='master_lobmk2_resp'):
    """
    Some more dirty tests of rapid response matrix reading.
    :param d:
    :return:
    """
    import pathlib
    import pickle
    import blosc
    # import gzip


    files = sorted(
        [x.name for x in pathlib.Path(d).glob(f"fresp_mat_gw_0_*.hdf")]
            )
    # test on file1
    f = files[0]
    print(f"Reading {f}")
    fname = pathlib.Path(d).joinpath(f)
    r = int(''.join([s for s in f if s.isdigit()]))
    t0 = time.perf_counter()
    with tables.open_file(fname, 'r') as h5_file:
        dat = h5_file.root[f'r_{r}'].read()
    t1 = time.perf_counter()
    print(f"Read hdf took {t1 - t0}s \n")

    # # clear standby.....
    # t = np.random.random((int(5e7),100))
    # t = None
    # t = np.random.random((int(5e7), 100))
    # t = None
    # save to numpy file
    fname = fname.with_suffix('.npy')
    print(f"Saving to numpy {fname.name}")
    np.save(fname, dat)
    print(f"Reading numpy {fname.name}")
    t0 = time.perf_counter()
    datnp = np.load(fname)
    t1 = time.perf_counter()
    print(f"Read np took {t1 - t0}s")
    print(np.array_equal(dat, datnp), '\n')

    # DROPPING compressed numpy slow as - 5 s read
    # fname = fname.with_suffix('.npz')
    # np.savez_compressed(fname, dat)
    # t0 = time.perf_counter()
    # dict_datnpz = np.load(fname)
    # datnpz = dict_datnpz['arr_0']
    # t1 = time.perf_counter()
    # print(f"Read np from compressed took {t1 - t0}s")

    # fname = fname.with_suffix('.pkl')
    # print(f"Saving to pickle {fname.name}")
    # with open(fname, 'wb') as pkl_file:
    #     pickle.dump(dat, pkl_file)
    # print(f"Reading pickle {fname.name}")
    # t0 = time.perf_counter()
    # with open(fname, 'rb') as pkl_file:
    #     dat2 = pickle.load(pkl_file)
    # t1 = time.perf_counter()
    # print(f"Read np from pickle took {t1 - t0}s")
    # print(np.array_equal(dat, dat2), '\n')
    #
    # fname = fname.with_suffix('.bin')
    # print(f"Saving to uncomp binary {fname.name}")
    # arr = bytearray(dat)
    # with open(fname, 'wb') as fp:
    #     fp.write(arr)
    # print(f"Reading uncomp binary {fname.name}")
    # t0 = time.perf_counter()
    # with open(fname, 'rb') as fp:
    #     ba = fp.read()
    # datb = np.frombuffer(ba, dtype=dat.dtype).reshape(dat.shape)
    # t1 = time.perf_counter()
    # print(f"Read np from bin took {t1 - t0}s")
    # print(np.array_equal(dat, datb), '\n')

    # DROPPING gzip - slow as 6 s read
    # fname = fname.with_suffix('.gz')
    # with gzip.open(fname, 'wb') as fp:
    #     fp.write(arr)
    # t0 = time.perf_counter()
    # with gzip.open(fname, 'rb') as fp:
    #     gz = fp.read()
    # datgz = np.frombuffer(gz, dtype=dat.dtype).reshape(dat.shape)
    # t1 = time.perf_counter()
    # print(f"Read np from gz took {t1 - t0}s")
    # print(np.array_equal(dat, datgz))

    fname = fname.with_suffix('.blc')
    print(f"Saving to compressed blosc binary {fname.name}")
    c = blosc.compress_ptr(dat.__array_interface__['data'][0], dat.size,
                           dat.dtype.itemsize, clevel=9, cname='zstd',
                           shuffle=blosc.SHUFFLE)
    # c = blosc.compress_ptr(dat.__array_interface__['data'][0], dat.size,
    #                        dat.dtype.itemsize, clevel=9, cname='lz4',
    #                        shuffle=blosc.SHUFFLE)
    with open(fname, 'wb') as fp:
        pickle.dump((dat.shape, dat.dtype), fp)
        fp.write(c)

    print(f"Reading compressed blosc binary no pre_alloc {fname.name}")
    t0 = time.perf_counter()
    with open(fname, 'rb') as fp:
        shape, dtype = pickle.load(fp)
        c = fp.read()
    datbl = np.empty(shape, dtype)
    blosc.decompress_ptr(c, datbl.__array_interface__['data'][0])
    t1 = time.perf_counter()
    print(f"Read np from blosc took {t1 - t0}s")
    print(np.array_equal(dat, datbl), '\n')

    print(f"Reading compressed blosc binary with pre_alloc {fname.name}")
    datbl2 = dat.copy() * 100
    t0 = time.perf_counter()
    with open(fname, 'rb') as fp:
        shape, dtype = pickle.load(fp)
        c = fp.read()
    blosc.decompress_ptr(c, datbl2.__array_interface__['data'][0])
    t1 = time.perf_counter()
    print(f"Read np from blosc pre_allocated took {t1 - t0}s")
    print(np.array_equal(dat, datbl2), '\n')

    datbl3 = dat.copy() * 100
    fname = fname.with_stem(fname.stem + "_n")
    print(f"Reading new blosc binary {fname.name}")
    t0 = time.perf_counter()
    with open(fname, 'rb') as fp:
        shape, dtype = pickle.load(fp)
        c = fp.read()
    # datbl3 = np.empty(shape, dtype)
    blosc.decompress_ptr(c, datbl3.__array_interface__['data'][0])
    t1 = time.perf_counter()
    print(f"Read np from new blosc took {t1 - t0}s")
    print(np.array_equal(dat, datbl3), '\n')

    test=None


def make_shortname_versions():
    # A - response matrix
    a_d = "master_basic_resp"
    new_a_d = "A"
    j_d = "master_fosm"

    pst = pyemu.Pst(os.path.join(a_d, "freyberg.pst"))
    print("Reading GW concentration output jcbs")
    gwoe_fnames = [fname for fname in os.listdir(a_d)
                   if all([s in fname for s in ['ucn', 'jcb']])]
    gwoe = []
    for i, oe_fname in enumerate(gwoe_fnames):
        print(f"    Reading jcb {i + 1}/{len(gwoe_fnames)}")
        gwoe.append(pyemu.ObservationEnsemble.from_binary(
            pst, os.path.join(a_d, oe_fname))._df.astype('float32').T)
    gwoe = pd.concat(gwoe)
    runid = pd.read_csv(os.path.join(a_d, "runid.csv"),
                        index_col=0).rename(columns={1: 'runo'})
    runid.index = runid.index.astype(str)
    gwoe.rename(columns=runid.iloc[:, 0].to_dict())
    spike_reals = pd.read_csv(os.path.join(a_d, "resp_reals.csv"),
                              index_col=0,
                              header=None).rename(columns={1: 'name'})
    pars = pst.parameter_data.loc[spike_reals.name].copy()
    obs = pst.observation_data
    trimobs = obs.loc[obs.obsnme.str.endswith('364')].obsnme
    gwoe = gwoe.loc[trimobs]
    spike_reals['new_name'] = spike_reals.name.apply(
        lambda x:
        x.split('_x')[0].replace(
            'multiplier_load_inst:0', "load").replace(
            'i:', '').replace('j:', '')
    )
    pars.loc[:, ['parnme']] = pars.parnme.map(
        spike_reals.set_index('name').new_name.to_dict())
    spike_reals.index = spike_reals.index.astype(str)
    gwoe = gwoe.rename(columns=runid.iloc[:, 0].to_dict())
    baserun = gwoe['BASE']
    roe = gwoe.sub(baserun, axis=0).drop('BASE', axis=1).rename(
        columns=spike_reals.new_name.to_dict())/10.
    jco = pyemu.Jco.from_dataframe(roe)
    baserun = baserun/10
    if not os.path.exists(new_a_d):
        os.mkdir(new_a_d)
    baserun.rename_axis('obsnme').to_csv(os.path.join(new_a_d, "Abaseobs.dat"), sep=' ')
    jco.to_binary(os.path.join(new_a_d, "A.jcb"))
    jco.to_ascii(os.path.join(new_a_d, "A.mat"))
    pst.parameter_data = pars
    pst.observation_data = obs.loc[trimobs]
    pst.write(os.path.join(new_a_d, "A.pst"), version=1)

    # Jacobian
    pst = pyemu.Pst(os.path.join(j_d, "freyberg.pst"))
    pst.res.loc[trimobs].modelled.to_csv(os.path.join(new_a_d, "Jbaseobs.dat"), sep=' ')
    jcb = pyemu.Jco.from_binary(os.path.join(j_d, "freyberg.jcb")).to_dataframe()
    pars = pst.parameter_data
    loadpars = pars.index.str.startswith('multiplier_load')
    runidpar = pars.index.str.startswith('runid')
    pars = pst.parameter_data.loc[~(loadpars | runidpar)].copy()
    pars.loc[:, 'parnme'] = [f'p{n:03d}' for n in range(len(pars))]
    jcb = jcb.loc[trimobs.values, pars.index].rename(columns=pars.parnme.to_dict())
    pars.parnme.to_csv(os.path.join(new_a_d, "parnme_map.csv"))
    jcb = pyemu.Jco.from_dataframe(jcb)
    jcb.to_binary(os.path.join(new_a_d, "J.jcb"))
    jcb.to_ascii(os.path.join(new_a_d, "J.mat"))
    pst.parameter_data = pars
    pst.observation_data = pst.observation_data.loc[trimobs]
    pst.write(os.path.join(new_a_d, "J.pst"), version=1)

    # Parcov
    cov = pyemu.Cov.from_parameter_data(pst, sigma_range=6.0)
    cov.to_binary(os.path.join(new_a_d, "C_diag.jcb"))
    cov.to_ascii(os.path.join(new_a_d, "C_diag.mat"))

    # Load
    load = pd.read_csv("test_load.csv", index_col=0)
    load.index = load.index.map(
        lambda x:
        x.split('_x')[0].replace(
            'multiplier_load_inst:0', "load").replace(
            'i:', '').replace('j:', '')
    )
    load.drop(['i', 'j'], axis=1).to_csv(os.path.join(new_a_d, "L.dat"),
                                         header=False, sep=' ')

    # full cov
    cov = pyemu.Cov.from_ascii(
        os.path.join("new_temp", "freyberg.prior.cov")).to_dataframe()
    cov = cov.loc[pars.index, pars.index].rename(index=pars.parnme.to_dict(),
                                                 columns=pars.parnme.to_dict())
    cov = pyemu.Cov.from_dataframe(cov)
    cov.to_binary(os.path.join(new_a_d, "C_full.jcb"))
    cov.to_ascii(os.path.join(new_a_d, "C_full.mat"))
    pass



if __name__ == "__main__":
    mp.freeze_support()
    # setup_freyberg_models()
    # freyberg_pst(nreals=100)
    # prep_for_response_ensemble_unc()
    # prep_for_condor('master_respunc', 'template_respunc',
    #                 pestpp_exe="pestpp-swp.exe",
    #                 bin_dir=os.path.join('..', 'exe'), stomp=True)
    # run_and_process('master_respunc', port=4004, run=True, process=True,
    #                 pestpp_exe="pestpp-swp.exe")
    # _,_,_ = test_process_response_unc_res('master_respunc',
    #                                       load_all_csv=False,
    #                                       pcts=None, cdfplots=True)
    # basic(run=True, build_respmat=True, nreals=100)
    # benchmark(run=True, low_memory=False, nreals=100)
    # lo_benchmark(run=True, low_memory=True, loworder=4, t_d='template_lobmk',
    #              nreals=100)
    # fosm_stds(runjco=True)

    # invest_unc_scaling(run=True, rd='plusminus')
    # invest_unc_scaling(run=True, rd='spikevar')
    # invest_unc_scaling(run=True, rd='dirac')
    # invest_unc_scaling(run=True, rd='spikerange')

    make_shortname_versions()
    # m_d = 'master_basic_resp'
    # resp_matrix_from_oe(m_d)
    # m_d = 'master_bmk_resp'
    # resp_matrix_from_oe(m_d)
    # m_d = 'master_lobmk_resp'
    # resp_matrix_from_oe(m_d, process_lo_matrix=True, low_memory=True)
    # pstfile = 'freyberg.pst'
    # pst = pyemu.Pst(os.path.join(t_d, pstfile))
    # pe = pyemu.ParameterEnsemble.from_binary(pst, os.path.join(t_d, f'par.jcb'))
    # test = None
    # resp_matrix_from_oe('master_lobmk2_resp', process_lo_matrix=True, low_memory=True)
    # fullstd = get_respmat_std(
    #     os.path.join("master_lobmk2_resp", "fresp_mat_gw.blc"),
    #     os.path.join("master_lobmk2_resp", "mean_fresp_mat_gw.hdf")
    # )
    # resp_martix_from_oe('test')
    # test_apply_load(os.path.join('..','transient_sire'), up2k=1)
    # test_readwrite(complevel=9, complib='blosc:lz4hc')
    # response_read_test(os.path.join('..', 'transient_sire'))
    # mp_share_test('np')
    # import pathlib
    # d = os.path.join('..', 'transient_sire')
    # files = sorted(
    #     [x.name for x in pathlib.Path(d).glob(f"fresp_mat_gw_0_??.hdf")]
    #         )
    # write_table(d, files, complevel=9, complib='blosc:lz4hc')
    pass
